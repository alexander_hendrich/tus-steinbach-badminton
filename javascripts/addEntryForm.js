$player1Name		= $('#addEntryPlayer1Name');
$player1NameLabel	= $('#addEntryPlayer1NameLabel');
$player1Squad		= $('#addEntryPlayer1Squad');
$player1SquadLabel	= $('#addEntryPlayer1SquadLabel');
$player2Name		= $('#addEntryPlayer2Name');
$player2NameLabel	= $('#addEntryPlayer2NameLabel');
$player2Squad		= $('#addEntryPlayer2Squad');
$player2SquadLabel	= $('#addEntryPlayer2SquadLabel');

$selectDiscipline1	= $('#addEntryDisciplineID');
$selectDiscipline2	= $('#disciplineTableChooser');

$disciplineTables	= $('.disciplineTable');

function updateAddEntryForm(id) {
	//hide all disciplineTables
	$disciplineTables.hide();
	
	//show selected disciplineTable
	$('#disciplineTable' + id).show();
	
	//show single/double inputs
	if(id === '1' || id === '3' || id === '6' || id === '8') {
		//show singles
		$player1NameLabel.html('Name:');
		$player1SquadLabel.html('Verein:');
		$player2Name.hide();
		$player2NameLabel.hide();
		$player2Squad.hide();
		$player2SquadLabel.hide();
		$player2Name.val('');
		$player2Squad.val('');
	} else {
		//show doubles
		$player1NameLabel.html('Spieler 1 Name:');
		$player1SquadLabel.html('Spieler 1 Verein:');
		$player2Name.show();
		$player2NameLabel.show();
		$player2Squad.show();
		$player2SquadLabel.show();
	}
	
	//update all selects
	$selectDiscipline1.val(id);
	$selectDiscipline2.val(id);
}

$(document).ready(function() {
	var preSelectedID = $selectDiscipline1.val();

	//init
	updateAddEntryForm(preSelectedID);
});

$selectDiscipline1.change(function() {
	var id = $(this).val();
	
	updateAddEntryForm(id);
});

$selectDiscipline2.change(function() {
	var id = $(this).val();
	
	updateAddEntryForm(id);
});