var $addCommentTextArea		= $('#addCommentText');
var $addCommentTextCounter	= $('#addCommentTextCounter');
var maxLength				= parseInt($addCommentTextArea.attr('maxlength'), 10);


$addCommentTextArea.keyup(function() {
	//cut text
	if($addCommentTextArea.val().length > maxLength) {
		$addCommentTextArea.val($addCommentTextArea.val().substr(0, maxLength));
	}
	
	//update count
	var numRest = maxLength - $addCommentTextArea.val().length;
	$addCommentTextCounter.html('(noch ' + numRest + ' Zeichen)');
});