var $slideShowImage					= $('#slideShowImage');
var $slideShowBackground			= $('#slideShowBackground');
var $slideShowNextSlideButton		= $('#slideShowNextSlideButton');
var $slideShowPreviousSlideButton	= $('#slideShowPreviousSlideButton');
var $slideShowWrapper				= $('#slideShowWrapper');
var $slideShowCurrentThumbListItem	= null;

function slideShowUpdateImage() {
	//hide current image
	$slideShowWrapper.css('visibility', 'hidden');
	
	//get image url
	var imageUrl = $slideShowCurrentThumbListItem.children('a').attr('href');
	
	//set new image
	$slideShowImage.attr('src', imageUrl);
	$slideShowWrapper.css('visibility', 'visible');
}

function slideShowNextImage() {
	//get nextThumb
	$slideShowCurrentThumbListItem	= ($slideShowCurrentThumbListItem.next().length > 0) ? $slideShowCurrentThumbListItem.next() : $slideShowCurrentThumbListItem.parent().children(':first');

	//show image
	slideShowUpdateImage();
}

function slideShowPreviousImage() {
	//get previousThumb
	$slideShowCurrentThumbListItem	= ($slideShowCurrentThumbListItem.prev().length > 0) ? $slideShowCurrentThumbListItem.prev() : $slideShowCurrentThumbListItem.parent().children(':last');

	//show image
	slideShowUpdateImage();
}

function slideShowStart() {
	//show background
	$slideShowBackground.fadeIn(500);
	
	//show image
	slideShowUpdateImage();
	
	//bind key-navigation
	$(document).keyup(function(event) {
		var keyCode = event.which;
	
		if(keyCode === 37) {
			slideShowPreviousImage();
		} else if(keyCode === 39) {
			slideShowNextImage();
		}
		event.preventDefault();
	});
}

function slideShowEnd() {
	//hide background
		$slideShowBackground.fadeOut(500);
		
		//reset image
		$slideShowImage.attr('src', '');
		
		//unbind key-navigation
		$(document).keydown(function() {});
}

//auto center slideShowImage depending on imagesize
$slideShowImage.load(function() {
	var width	= $slideShowImage.width();
	var height	= $slideShowImage.height();
	$slideShowWrapper.css('margin-top', -(height/2) + 'px');
	$slideShowWrapper.css('margin-left', -(width/2) + 'px');
});

//start slideShow
$('ul.galleryThumbs li a').click(function(event) {
	$slideShowCurrentThumbListItem = $(this).parent();
	
	slideShowStart();

	event.preventDefault();
});

//end slideShow
$slideShowBackground.click(function(e) {
	if(!$(e.target).is($slideShowPreviousSlideButton) && !$(e.target).is($slideShowNextSlideButton)) {
		slideShowEnd();
	}
});

//nextSlideButton
$slideShowNextSlideButton.click(function(event) {
	slideShowNextImage();
	event.preventDefault();
});

//previousSlideButton
$slideShowPreviousSlideButton.click(function(event) {
	slideShowPreviousImage();
	event.preventDefault();
});