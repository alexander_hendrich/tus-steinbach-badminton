$(document).ready(function() {
	$('#navigation ul.subNavigation').hide();	
});


$('#navigation li').hover(
	function () { //handler in
		if($.browser.msie && parseInt($.browser.version, 10) <= 7) {
			$(this).children('ul.subNavigation').css('display', 'block');
		} else {
			$(this).children('ul.subNavigation').stop(false, true).slideDown(200);
		}
	},
	function () { //handler out
		if($.browser.msie && parseInt($.browser.version, 10) <= 7) {
			$(this).children('ul.subNavigation').css('display', 'none');
		} else {
			$(this).children('ul.subNavigation').stop(false, true).slideUp(200);
		}
	}
);