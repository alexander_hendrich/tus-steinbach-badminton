$(document).ready(function() {
	$('.tournamentTable tbody').hide();
});

$('.tournamentTable a.details').click(function(e) {
	$tbody = $(this).parents('.tournamentTable').children('tbody');

	if($tbody.is(':visible')) {
		//hide
		$tbody.hide(500);
		$(this).html('Details einblenden');
	} else {
		//show
		$tbody.show(500);
		$(this).html('Details ausblenden');
	}
	
	e.preventDefault();
});