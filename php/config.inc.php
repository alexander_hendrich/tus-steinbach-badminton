<?php

	### DBHandler
	define('DB_HANDLER_LOGIN_HOST',				'localhost');
	define('DB_HANDLER_LOGIN_DB_NAME',			'usr_web588_2');
	define('DB_HANDLER_LOGIN_USER_NAME',		'web588');
	define('DB_HANDLER_LOGIN_USER_PASSWORD',																																															'NecRj3ie');


	### MailHandler
	define('MAIL_HANDLER_LOGIN_USER_NAME',			'web588p5');
	define('MAIL_HANDLER_LOGIN_USER_PASSWORD',																																															'NecRj3ie');
	define('MAIL_HANDLER_TRANSPORT_ADDRESS',		'mail.tus-steinbach-badminton.de');

	### NotificationHandler
	define('NOTIFICATION_HANDLER_RECEIVER_EMAIL',	'webmaster@tus-steinbach-badminton.de');
	define('NOTIFICATION_HANDLER_SENDER_EMAIL',		'no-reply@tus-steinbach-badminton.de');

	### NewsPage
	define('NEWS_PAGE_NUM_NEWS_ENTRIES_PER_PAGE',	5);

	### NewsEntryPage
	define('NEWS_ENTRY_PAGE_NUM_RELATED_NEWS_ENTRIES',	5);

	### TableNames
	define('TABLE_NAME_COMMENTS',					'comments');
	define('TABLE_NAME_DISCIPLINES',				'disciplines');
	define('TABLE_NAME_ENTRIES',					'entries');
	define('TABLE_NAME_GALLERIES',					'galleries');
	define('TABLE_NAME_GALLERY_IMAGES',				'galleryImages');
	define('TABLE_NAME_NEWS_ENTRIES',				'newsEntries');
	define('TABLE_NAME_NEWS_ENTRIES_TAGS_RELATION',	'newsEntriesTagsRelation');
	define('TABLE_NAME_PLAYERS',					'players');
	define('TABLE_NAME_SQUADS',						'squads');
	define('TABLE_NAME_TOURNAMENTS',				'tournaments');
	define('TABLE_NAME_USERS',						'users');

	### Spam-check
	define('SPAM_CHECK_HONEYPOT_NAME',	'url');
	define('SPAM_CHECK_TIME_NAME',		'startTime');
	define('SPAM_CHECK_MIN_TIME',		2);
	define('SPAM_CHECK_MAX_TIME',		60*60); //1 hour

	### general
	define('DOMAIN',				'tus-steinbach-badminton.de');
	define('ABSOLUTE_LINK',			'http://www.'.DOMAIN);
	define('MAIL_EOL',				"\r\n");
	define('MAIL_DIVIDER',			'+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++');

?>