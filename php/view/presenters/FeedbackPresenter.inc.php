<?php
	class FeedbackPresenter {
		### attributes
		private $feedbackHandler;
		private $types = array (
			'confirmation'	=> 'Hinweis',
			'error'			=> 'Fehler',
			'information'	=> 'Information',
			'warning'		=> 'Warnung'
		);
		
		### methodes
		
		public function __construct($feedbackHandler) {
			$this->feedbackHandler = $feedbackHandler;
		}
		
		public function getFeedbackBigHtml($topic, $location) {
			$html = '';
			foreach($this->types as $type=>$typeName) {
				if($this->feedbackHandler->isFeedback($type, $topic, $location)) {
					$message	= implode('<br />', array_reverse($this->feedbackHandler->getFeedbackMessages($type, $topic, $location)));
					
					$html .= '<div class="feedback '.$type.'">'
								.'<span class="type">'.$typeName.'</span>'
								.'<p class="message">'.$message.'</p>'
							.'</div>';
				}				
			}
			
			return $html;
		}
		
		public function getFeedbackSmallHtml($topic, $location) {
			$html = '';
			foreach($this->types as $type=>$typeName) {
				if($this->feedbackHandler->isFeedback($type, $topic, $location)) {
					$message	= implode('<br />', array_reverse($this->feedbackHandler->getFeedbackMessages($type, $topic, $location)));
					
					$html .= '<div class="feedback '.$type.'">'
								.'<p class="message">'.$message.'</p>'
							.'</div>';
				}				
			}
			
			return $html;
		}
	}
?>