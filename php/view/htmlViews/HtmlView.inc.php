<?php
	abstract class HtmlView {
		### attributes
		protected $title;
		protected $metaKeywords = array();
		protected $metaDescription;
		
		### methodes
		
		abstract public function getHtml();
		
		public static function formatStringToHtml($string) {
			$string = trim($string);
			$string = htmlspecialchars($string, ENT_COMPAT | ENT_HTML5, 'UTF-8');
			return $string;
		}
		
		public static function formatTextToHtml($text) {
			$text = self::formatStringToHtml($text);
			$text = nl2br($text);
			return $text;
		}
		
		public static function formatBBCodeTextToHtml($text) {			
			$text = self::formatTextToHtml($text);
			$text = preg_replace('#\[url=(.*)\](.*)\[/url\]#isU', "<a href=\"$1\">$2</a>", $text);
			$text = preg_replace('#\[url\](.*)\[/url\]#isU', "<a href=\"$1\">$1</a>", $text);
			
  			return $text;
		}
		
		public static function getSpamPreventionHtml() {
			$html = '<input type="hidden" name="startTime" value="'.time().'" />'
					.'<input type="text" name="url" value="" class="spamPrevention" />';
			
			return $html;
		}
		
		public static function secureEMailLink($email) {
			$obfuscatedEMail = strrev(str_rot13($email));
			
			$html = '<a class="secureEMailLink" href="mailto:'.$obfuscatedEMail.'">'.$obfuscatedEMail.'</a>';
			
			return $html;
		}
	}
?>