<?php
	class ManageDisciplineHtmlView extends AdminPageHtmlView {
		### attributes
		private $discipline;

		### methodes

		public function setPageData($pageData) {
			$this->discipline = $pageData['discipline'];
		}

		protected function getContentAreaHtml() {
			$discipline = $this->discipline;

			$group				= $discipline->getGroup();
			$type				= $discipline->getType();
			$name				= HtmlView::formatStringToHtml('Gruppe '.$group.' - '.$type);
			$numMaxEntries		= $discipline->getNumMaxEntries();
			$numEntriesAccepted	= $discipline->getNumEntriesAccepted();
			$numEntriesPending	= $discipline->getNumEntriesPending();
			$numEntriesWaiting	= $discipline->getNumEntriesWaiting();
			$isDisciplineFull	= $discipline->isFull();

			$entriesAccepted	= $discipline->getEntriesAccepted();
			$entriesPending		= $discipline->getEntriesPending();
			$entriesWaiting		= $discipline->getEntriesWaiting();


			$entriesTableHtml				= EntryUI::getAdminEntriesTableHtml($entriesAccepted, false, false);
			$entriesPendingTableHtml 		= EntryUI::getAdminEntriesTableHtml($entriesPending, !$isDisciplineFull, true);
			$entriesWaitinglistTableHtml 	= EntryUI::getAdminEntriesTableHtml($entriesWaiting, !$isDisciplineFull, false);

			$html = '<h2>Disziplin verwalten - '.$name.'</h2>'
					.$this->feedbackPresenter->getFeedbackBigHtml('entrySetStateAccepted', 'general')
					.$this->feedbackPresenter->getFeedbackBigHtml('entrySetStateWaiting', 'general')
					.'<h3>Offene Meldungen ['.$numEntriesPending.' offene Meldung(en)]</h3>'
					.$entriesPendingTableHtml
					.'<h3>Akzeptierte Meldungen ['.$numEntriesAccepted.' / '.$numMaxEntries.' Meldung(en)]</h3>'
					.$entriesTableHtml
					.'<h3>Warteliste Meldungen ['.$numEntriesWaiting.' wartende Meldung(en)]</h3>'
					.$entriesWaitinglistTableHtml;

			return $html;
		}
	}
?>