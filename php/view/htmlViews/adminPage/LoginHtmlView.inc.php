<?php
	class LoginHtmlView extends HtmlView {
		### attributes
		private $feedbackPresenter;


		### methodes
		#
		public function __construct($feedbackPresenter) {
			$this->feedbackPresenter = $feedbackPresenter;

			$this->title = 'Login - Adminbereich - TuS Steinbach Badminton';
		}

		public function getHtml() {
			$html = '<!DOCTYPE html>'
					.'<html>'
						.'<head>'
							.($this->getHeadHtml())
						.'</head>'
						.'<body>'
							.'<form method="post" action="" id="loginBox">'
								.'<h1>Admin-Login</h1>'
								.$this->feedbackPresenter->getFeedbackBigHtml('login', 'general')
								.'<label for="loginEMail">E-Mail:</label>'
								.'<input type="email" name="loginEMail" id="loginEMail" />'
								.'<label for="loginPassword">Passwort:</label>'
								.'<input type="password" name="loginPassword" id="loginPassword" />'
								.'<input type="submit" name="login" value="Anmelden" class="button" />'
							.'</form>'
						.'</body>'
					.'</html>';

			return $html;
		}

		private function getHeadHtml() {
			$html = '<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,700" rel="stylesheet" type="text/css">'
					.'<title>'.($this->title).'</title>'
					.'<meta charset="utf-8" />'
					.'<link rel="stylesheet" type="text/css" href="/css/admin.css" />'
					.'<!--[if lt IE 9]><script src="/javascripts/html5shiv.js"></script><![endif]-->';

			return $html;
		}

	}
?>