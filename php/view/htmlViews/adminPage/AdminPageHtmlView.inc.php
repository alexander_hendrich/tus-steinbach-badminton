<?php
	class AdminPageHtmlView extends HtmlView {
		### attributes
		protected $disciplines;

		protected $feedbackPresenter;

		### methodes

		public function __construct($feedbackPresenter, $disciplines) {
			$this->feedbackPresenter	= $feedbackPresenter;
			$this->disciplines			= $disciplines;

			$this->title = 'Adminbereich - TuS Steinbach Badminton';
		}

		public function getHtml() {
			$sidebarHtml			= $this->getSidebarHtml();
			$contentAreaHtml		= $this->getContentAreaHtml();

			$html = '<!DOCTYPE html>'
					.'<html>'
						.'<head>'
							.($this->getHeadHtml())
						.'</head>'
						.'<body>'
							.'<div id="centerWrapper">'
								.'<div id="sidebar">'
									.$sidebarHtml
								.'</div>'
								.'<div id="contentArea">'
									.$contentAreaHtml
								.'</div>'
							.'</div>'
						.'</body>'
					.'</html>';

			return $html;
		}

		private function getHeadHtml() {
			$html = '<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,700" rel="stylesheet" type="text/css">'
					.'<title>'.($this->title).'</title>'
					.'<meta charset="utf-8" />'
					.'<link rel="stylesheet" type="text/css" href="/css/admin.css" />'
					.'<!--[if lt IE 9]><script src="/javascripts/html5shiv.js"></script><![endif]-->';

			return $html;
		}

		private function getSidebarHtml() {
			$disciplineListHtml = DisciplineUI::getAdminNavigationListHtml($this->disciplines);

			$html = '<header id="header">'
						.'<h1>TuS Steinbach Badminton Adminbereich</h1>'
					.'</header>'
					.'<nav id="navigation">'
						.'<h6>News</h6>'
						.'<ul>'
							.'<li><a href="/admin/newsEntries/add/">hinzufügen</a></li>'
							.'<li><a href="/admin/newsEntries/">bearbeiten/löschen</a></li>'
						.'</ul>'
						.'<h6>Mannschaften</h6>'
						.'<ul>'
							.'<li><a href="/admin/squads/add/">hinzufügen</a></li>'
							.'<li><a href="/admin/squads/">bearbeiten/löschen</a></li>'
						.'</ul>'
						.'<h6>Turniere</h6>'
						.'<ul>'
							.'<li><a href="/admin/tournaments/add/">hinzufügen</a></li>'
							.'<li><a href="/admin/tournaments/">bearbeiten/löschen</a></li>'
						.'</ul>'
						.'<h6>Altkönigturnier</h6>'
						.$disciplineListHtml
					.'</navigation>';

			return $html;
		}

		protected function getContentAreaHtml() {
			$html = '';

			return $html;
		}
	}
?>