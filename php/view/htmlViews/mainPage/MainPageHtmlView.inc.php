<?php
	abstract class MainPageHtmlView extends HtmlView {
		### attributes
		protected $activeMenuItem;
		protected $showSlideShowOverlay = false;

		### methodes

		public function getHtml() {
			$navigationHtml			= $this->getNavigationHtml();
			$contentAreaHtml		= $this->getContentAreaHtml();
			$footerHtml				= $this->getFooterHtml();
			$slideShowOverlaHtml	= $this->getSlideShowOverlayHtml();
			$javascriptHtml			= $this->getJavascriptHtml();

			$html = '<!DOCTYPE html>'
					.'<html>'
						.'<head>'
							.($this->getHeadHtml())
						.'</head>'
						.'<body>'
							.'<div id="stickyFooterContent">'
								.'<div id="centerWrapper">'
									.'<div id="imageBar">'

									.'</div>'
									.'<header id="header">'
										.'<h1><a href="/">TuS Steinbach Badminton</a></h1>'
										.'<nav id="navigation">'
											.$navigationHtml
										.'</nav>'
									.'</header>'
									.'<div id="contentArea">'
										.$contentAreaHtml
									.'</div>'
								.'</div>'
								.'<div id="stickyFooterPlaceholder"></div>'
							.'</div>'
							.'<footer id="footer" class="footer">'
								.$footerHtml
							.'</footer>'
							.$slideShowOverlaHtml
							.$javascriptHtml
						.'</body>'
					.'</html>';

			return $html;
		}

		private function getHeadHtml() {
			$html = '<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,700" rel="stylesheet" type="text/css">'
					.'<title>'.($this->title).'</title>'
					.'<meta charset="utf-8" />'
					.'<link rel="stylesheet" type="text/css" href="/css/master.css" />'
					.'<meta name="description" content="'.$this->metaDescription.'" />'
					.'<meta name="keywords" content="'.implode(', ', $this->metaKeywords).'" />'
					.'<meta name="robots" content="index,follow" />'
					.'<meta name="author" content="Alexander Hendrich, Webdesign-Hendrich.de" />'
					.'<!--[if lt IE 9]><script src="/javascripts/html5shiv.js"></script><![endif]-->';

			return $html;
		}

		private function getNavigationHtml() {
			$html = '<ul>'
						.'<li><a href="/" class="'.($this->activeMenuItem == 'news' ? 'active' : '').'">News</a></li>'
						.'<li>'
							.'<span class="'.($this->activeMenuItem == 'squads' ? 'active' : '').'">Mannschaften</span>'
							.'<ul class="subNavigation">'
								.'<li><a href="/mannschaften/senioren/">Senioren</a></li>'
								.'<li><a href="/mannschaften/jugend/">Jugend</a></li>'
							.'</ul>'
						.'</li>'
						.'<li><a href="/training/" class="'.($this->activeMenuItem == 'training' ? 'active' : '').'">Training</a></li>'
						.'<li>'
							.'<span class="'.($this->activeMenuItem == 'tournament' ? 'active' : '').'">Altkönigturnier</span>'
							.'<ul class="subNavigation">'
								.'<li><a href="/altkoenigturnier/ausschreibung/">Ausschreibung</a></li>'
								.'<li><a href="/altkoenigturnier/meldeliste/">Meldeliste</a></li>'
								.'<li><a href="/altkoenigturnier/ergebnisse/">Ergebnisse 2012</a></li>'
								.'<li><a href="/news/19-05-2012-1-steinbacher-badminton-altkoenigturnier/">Bericht 2012</a></li>'
							.'</ul>'
						.'</li>'
						.'<li><a href="/turniere/" class="'.($this->activeMenuItem == 'tournaments' ? 'active' : '').'">Turniere</a></li>'
						.'<li><a href="/kontakt/" class="'.($this->activeMenuItem == 'contact' ? 'active' : '').'">Kontakt</a></li>'
					.'</ul>';

			return $html;
		}

		abstract protected function getContentAreaHtml();

		private function getFooterHtml() {
			$html =	'<div class="gridContainer">'
						.'<span class="left">Webdesign von <a href="http://www.webdesign-hendrich.de">www.webdesign-hendrich.de</a></span>'
						.'<span class="right"><a href="/kontakt/">Kontakt</a><a href="/impressum/">Impressum</a></span>'
					.'</div>';

			return $html;
		}

		private function getSlideShowOverlayHtml() {
			if($this->showSlideShowOverlay) {
				$html = '<div class="slideShowBackground" id="slideShowBackground">'
							.'<div class="slideShowWrapper" id="slideShowWrapper">'
								.'<img src="" class="slideShowImage" id="slideShowImage" />'
								.'<a href="#" class="slideShowPreviousSlideButton" id="slideShowPreviousSlideButton">&lt;</a>'
								.'<a href="#" class="slideShowNextSlideButton" id="slideShowNextSlideButton">&gt;</a>'
							.'</div>'
						.'</div>';
			} else {
				$html = '';
			}

			return $html;
		}

		private function getJavascriptHtml() {
			$html = '<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>'
					.'<script type="text/javascript" src="/javascripts/master-ck.js"></script>'
					.'<script type="text/javascript"> var _gaq = _gaq || []; _gaq.push(["_setAccount", "UA-4543319-8"]); _gaq.push(["_trackPageview"]); (function() { var ga = document.createElement("script"); ga.type = "text/javascript"; ga.async = true; ga.src = ("https:" == document.location.protocol ? "https://ssl" : "http://www") + ".google-analytics.com/ga.js"; var s = document.getElementsByTagName("script")[0]; s.parentNode.insertBefore(ga, s); })(); </script>';

			return $html;
		}
	}
?>