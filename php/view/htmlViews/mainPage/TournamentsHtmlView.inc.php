<?php
	class TournamentsHtmlView extends MainPageHtmlView{
		### attributes
		private $tournaments;

		### methodes

		public function __construct() {
			//init meta data
			$this->title 			= 'Turniere - Badminton TuS Steinbach';
			$this->metaDescription 	= 'Private Badmintonturniere im Raum Frankfurt, Rhein-Main, Hessen, Rheinlandpfalz und Baden-Württemberg';
			$this->metaKeywords 	= array('TuS Steinbach', 'Badminton', 'Turniere', 'Privateturniere', 'Hobby', 'Frnkfurt', 'Hessen');
			$this->activeMenuItem 	= 'tournaments';
		}

		public function setPageData($tournaments) {
			$this->tournaments = $tournaments;
		}

		protected function getContentAreaHtml() {

			$html = $this->getTournamentsSectionHtml()
					.$this->getSidebarSectionHtml();

			return $html;
		}

		private function getTournamentsSectionHtml() {
			$tournamentsListHtml = TournamentUI::getTournamentsListHtml($this->tournaments);

			$html = '<article class="mainContentLeft contentBox">'
						.'<h2>Turniere</h2>'
						.'<p>Hier finden Sie eine Auswahl an Badminton-Turnieren in der Umgebung.</p>'
						.$tournamentsListHtml
						.'<p>Weitere Turniere finden Sie auf <a href="http://www.tv-badminton.de/index.php?page=125">www.tv-badminton.de</a>, <a href="http://www.turnierinfo.de/">www.turnierinfo.de</a> und <a href="http://badmintonfreunde.de/t2012sen/list.php">www.badmintonfreunde.de</a>.</p>'
					.'</article>';

			return $html;
		}

		private function getSidebarSectionHtml() {
			$html = '<aside class="sidebarRight sidebarBox">'
						.'<h3>Turnier anmelden</h3>'
						.'<p>Sie veranstalten ein Turnier in der Umgebung und möchten gerne in die Liste mit aufgenommen werden? Dann schreiben Sie uns eine E-Mail mit Ausschreibung (als PDF) an '.HtmlView::secureEMailLink('webmaster@tus-steinbach-badminton.de').'.</p>'
					.'</aside>';

			return $html;
		}
	}
?>