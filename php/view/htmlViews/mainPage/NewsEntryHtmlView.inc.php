<?php
	class NewsEntryHtmlView extends MainPageHtmlView{
		### attributes
		private $newsEntry;
		private $addCommentDefaultValues;
		private $feedbackPresenter;

		### methodes

		public function __construct($feedbackPresenter) {
			$this->feedbackPresenter		= $feedbackPresenter;

			//init meta-data
			$this->title			= 'News - TuS Steinbach Badminton';
			$this->metaDescription 	= '';
			$this->metaKeywords 	= array('TuS Steinbach', 'Badminton', 'News', 'Spielbericht', 'Turnierbericht');
			$this->activeMenuItem	= 'news';
		}

		public function setPageData($newsEntry, $addCommentDefaultValues) {
			$this->newsEntry 				= $newsEntry;
			$this->addCommentDefaultValues	= $addCommentDefaultValues;

			//update meta-data
			$headline	= HtmlView::formatStringToHtml($newsEntry->getHeadline());
			$summary	= HtmlView::formatStringToHtml($newsEntry->getSummary());
			$this->title			= $headline.' - News - TuS Steinbach Badminton';
			$this->metaDescription	= $summary;
		}

		protected function getContentAreaHtml() {
			$articleSectionHtml	= $this->getArticleSectionHtml();
			$sidebarSectionHtml	= $this->getSidebarSectionHtml();
			$commentSectionHtml = $this->getCommentSectionHtml();


			$html = $articleSectionHtml
					.$sidebarSectionHtml
					.$commentSectionHtml;

			return $html;
		}

		private function getArticleSectionHtml() {
			$newsEntryUI = new NewsEntryUI($this->newsEntry);

			$articleSectionHtml = $newsEntryUI->getArticleSectionHtml();

			$html = '<section class="mainContentLeft">'
						.$articleSectionHtml
					.'</section>';
			return $html;
		}

		private function getSidebarSectionHtml() {

			$relatedNewsEntriesSectionHtml	= $this->getRelatedNewsEntriesSectionHtml();

			if($this->newsEntry->hasGallery()) {
				$this->showSlideShowOverlay = true;
				$gallery = $this->newsEntry->getGallery();
				$galleryUI = new GalleryUI($gallery);

				$thumbsListHtml = $galleryUI->getThumbListHtml();
			} else {
				$thumbsListHtml = '';
			}

			$html = '<aside class="sidebarRight">'
						.$thumbsListHtml
						.$relatedNewsEntriesSectionHtml
					.'</aside>';

			return $html;
		}

		private function getGallerySectionHtml() {
		}

		private function getRelatedNewsEntriesSectionHtml() {
			$newsEntryUI = new NewsEntryUI($this->newsEntry);

			$relatedNewsEntriesListHtml = $newsEntryUI->getRelatedNewsEntriesListHtml();

			$html = '<section class="contentBox">'
						.'<h3>Ähnliche Beiträge</h3>'
						.$relatedNewsEntriesListHtml
					.'</section>';

			return $html;
		}

		private function getCommentSectionHtml() {
			$commentListSectionHtml	= $this->getCommentListSectionHtml();
			$commentAddSectionHtml 	= $this->getCommentAddSectionHtml();

			$html = $commentListSectionHtml
					.$commentAddSectionHtml;

			return $html;
		}

		private function getCommentListSectionHtml() {
			if($this->newsEntry->hasComments()) {
				$commentListHtml = CommentUI::getCommentListHtml($this->newsEntry->getComments());
			} else {
				$commentListHtml = '<p>Dieser Beitrag hat noch keine Kommentare.</p>';
			}

			$html = '<section class="mainContentLeft contentBox">'
						.'<h3>Kommentare</h3>'
						.$commentListHtml
					.'</section>';

			return $html;
		}

		private function getCommentAddSectionHtml() {
			$newsEntryID	= HtmlView::formatStringToHtml($this->addCommentDefaultValues['newsEntryID']);
			$authorName		= HtmlView::formatStringToHtml($this->addCommentDefaultValues['authorName']);
			$authorEMail	= HtmlView::formatStringToHtml($this->addCommentDefaultValues['authorEMail']);
			$text			= HtmlView::formatStringToHtml($this->addCommentDefaultValues['text']);

			$html = '<aside class="sidebarRight">'
						.'<section class="sidebarBox">'
							.'<h3>Kommentar schreiben</h3>'
							.'<form method="POST" action="#addComment" class="sidebarForm" id="addComment">'
								.$this->feedbackPresenter->getFeedbackBigHtml('addComment', 'general')

								.'<label for="addCommentAuthorName">Name</label>'
								.'<input type="text" name="addCommentAuthorName" id="addCommentAuthorName" placeholder="Name" value="'.$authorName.'" />'
								.$this->feedbackPresenter->getFeedbackSmallHtml('addComment', 'authorName')

								.'<label for="addCommentAuthorEMail">E-Mail <span class="inputInfo">(wird nicht veröffentlicht)</span></label>'
								.'<input type="email" name="addCommentAuthorEMail" id="addCommentAuthorEMail" placeholder="E-Mail" value="'.$authorEMail.'" />'
								.$this->feedbackPresenter->getFeedbackSmallHtml('addComment', 'authorEMail')

								.'<label for="addCommentText">Kommentar <span class="inputInfo" id="addCommentTextCounter">(noch 1000 Zeichen)</span></label>'
								.'<textarea name="addCommentText" id="addCommentText" maxlength="1000">'.$text.'</textarea>'
								.$this->feedbackPresenter->getFeedbackSmallHtml('addComment', 'text')

								.'<input type="submit" name="addComment" value="Kommentar schreiben" class="button" />'

								.'<input type="hidden" name="addCommentNewsEntryID" value="'.$newsEntryID.'" />'

								.HtmlView::getSpamPreventionHtml()
							.'</form>'
						.'</section>'
					.'</aside>';

			return $html;
		}

	}
?>