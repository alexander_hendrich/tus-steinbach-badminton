<?php
	class TournamentResultsHtmlView extends MainPageHtmlView {
		### attributes


		### methodes


		public function __construct() {
			//init meta data
			$this->title 			= 'Ergebnisse - Altkönigturnier 2012 - Badminton TuS Steinbach';
			$this->metaDescription 	= 'Die Sieger vom letzten Steinbacher Altkönigturnier.';
			$this->metaKeywords 	= array('Ergebnisse', 'Altkönigturnier', 'Ausschreibung', 'Badmintonturnier', 'Frankfurt', 'Hobby', 'Turnier', 'Badminton', 'TuS Steinbach');
			$this->activeMenuItem 	= 'tournament';
		}

		protected function getContentAreaHtml() {

			$html = $this->getResultsSectionHtml()
					.$this->getSidebarSectionHtml();

			return $html;
		}

		private function getResultsSectionHtml() {

			$html = '<article class="mainContentLeft contentBox">'
						.'<h2>Ergebnisse - 1. Steinbacher Altkönigturnier</h2>'
						.'<section>'
							.'<h3>Gruppe 1 - DD</h3>'
							.'<table class="horizontalTable">'
								.'<tr>'
									.'<th>Platzierung</th>'
									.'<th>Spieler</th>'
									.'<th>Verein</th>'
								.'<tr>'
								.'<tr>'
									.'<td class="firstPlace">1. Platz</td>'
									.'<td>Jasmin Greissl<br />Britta Röpke</td>'
									.'<td>Sportiv Limburgerhof<br />Sportiv Limburgerhof</td>'
								.'</tr>'
							.'</table>'
						.'</section>'
						.'<section>'
							.'<h3>Gruppe 1 - HD</h3>'
							.'<table class="horizontalTable">'
								.'<tr>'
									.'<th>Platzierung</th>'
									.'<th>Spieler</th>'
									.'<th>Verein</th>'
								.'<tr>'
								.'<tr>'
									.'<td class="firstPlace">1. Platz</td>'
									.'<td>Jonathan Krämer<br />Markus Peterka</td>'
									.'<td>TSG Frankfurt-Oberrad<br />TSG Frankfurt Oberrad</td>'
								.'</tr>'
								.'<tr>'
									.'<td class="secondPlace">2. Platz</td>'
									.'<td>Daniel Scheffler<br />Alexander Hendrich</td>'
									.'<td>TuS Steinbach<br />Tus Steinbach</td>'
								.'</tr>'
								.'<tr>'
									.'<td class="thirdPlace">3. Platz</td>'
									.'<td>Axel Greissl<br />Martin Stumpf</td>'
									.'<td>Sportiv Limburgerhof<br />Sportiv Limburgerhof</td>'
								.'</tr>'
							.'</table>'
						.'</section>'
						.'<section>'
							.'<h3>Gruppe 1 - MIX</h3>'
							.'<table class="horizontalTable">'
								.'<tr>'
									.'<th>Platzierung</th>'
									.'<th>Spieler</th>'
									.'<th>Verein</th>'
								.'<tr>'
								.'<tr>'
									.'<td class="firstPlace">1. Platz</td>'
									.'<td>Britta Röpke<br />Martin Stumpf</td>'
									.'<td>Sportiv Limburgerhof<br />Sportiv Limburgerhof</td>'
								.'</tr>'
								.'<tr>'
									.'<td class="secondPlace">2. Platz</td>'
									.'<td>Britta Arr-You<br />Daniel Scheffler</td>'
									.'<td>TuS Steinbach<br />TuS Steinbach</td>'
								.'</tr>'
								.'<tr>'
									.'<td class="thirdPlace">3. Platz</td>'
									.'<td>Jasmin Greissl<br />Axel Greissl</td>'
									.'<td>Sportiv Limburgerhof<br />Sportiv Limburgerhof</td>'
								.'</tr>'
							.'</table>'
						.'</section>'
						.'<section>'
							.'<h3>Gruppe 2 - DD</h3>'
							.'<table class="horizontalTable">'
								.'<tr>'
									.'<th>Platzierung</th>'
									.'<th>Spieler</th>'
									.'<th>Verein</th>'
								.'<tr>'
								.'<tr>'
									.'<td class="firstPlace">1. Platz</td>'
									.'<td>Helen Chaudhuri<br />Katharina Wolf</td>'
									.'<td>TGS Jügesheim<br />TGS Jügesheim</td>'
								.'</tr>'
								.'<tr>'
									.'<td class="secondPlace">2. Platz</td>'
									.'<td>Ivonne Richard<br />Anne Rühl</td>'
									.'<td>Spvgg Hattstein<br />Spvgg Hattstein</td>'
								.'</tr>'
								.'<tr>'
									.'<td class="thirdPlace">3. Platz</td>'
									.'<td>Vanessa Schreitz<br />Marion Schönherr</td>'
									.'<td>TuS Steinbach<br />TuS Steinbach</td>'
								.'</tr>'
							.'</table>'
						.'</section>'
						.'<section>'
							.'<h3>Gruppe 2 - HD</h3>'
							.'<table class="horizontalTable">'
								.'<tr>'
									.'<th>Platzierung</th>'
									.'<th>Spieler</th>'
									.'<th>Verein</th>'
								.'<tr>'
								.'<tr>'
									.'<td class="firstPlace">1. Platz</td>'
									.'<td>Timo Mossbeck<br />Dragan Subaric</td>'
									.'<td>Hobby<br />Hobby</td>'
								.'</tr>'
								.'<tr>'
									.'<td class="secondPlace">2. Platz</td>'
									.'<td>Marco Funk<br />Marcus Soworka</td>'
									.'<td>TTC Seligenstadt<br />TTC Seligenstadt</td>'
								.'</tr>'
								.'<tr>'
									.'<td class="thirdPlace">3. Platz</td>'
									.'<td>Nico Kirchner<br />Sundeep Matharu</td>'
									.'<td>TV Kalbach<br />1. Frankfurter BC</td>'
								.'</tr>'
							.'</table>'
						.'</section>'
						.'<section>'
							.'<h3>Gruppe 2 - MIX</h3>'
							.'<table class="horizontalTable">'
								.'<tr>'
									.'<th>Platzierung</th>'
									.'<th>Spieler</th>'
									.'<th>Verein</th>'
								.'<tr>'
								.'<tr>'
									.'<td class="firstPlace">1. Platz</td>'
									.'<td>Kristina Schubert<br />Konrad Hagemann</td>'
									.'<td>ABC Frankfurt<br />ABC Frankfurt</td>'
								.'</tr>'
								.'<tr>'
									.'<td class="secondPlace">2. Platz</td>'
									.'<td>Heike Schwab<br />Konrad Schäfer</td>'
									.'<td>TuS Steinbach<br />TuS Steinbach</td>'
								.'</tr>'
								.'<tr>'
									.'<td class="thirdPlace">3. Platz</td>'
									.'<td>Fashia Haq<br />Satish Kumar</td>'
									.'<td>SG Deutsche Bank Frankfurt<br />SG Deutsche Bank Frankfurt</td>'
								.'</tr>'
							.'</table>'
						.'</section>'
					.'</article>';

			return $html;
		}

		private function getSidebarSectionHtml() {
			$html = '<aside class="sidebarRight sidebarBox">'
						.'<h3>Wieder dabei?</h3>'
						.'<p>Sie möchten Ihren Titel verteidigen oder haben das letzte Turnier verpasst? Dann melden Sie sich jetzt an zur nächsten Ausgabe des Altkönigturniers!</p>'
						.'<a href="/altkoenigturnier/meldeliste/" class="button">Anmeldung</a>'
					.'</aside>';

			return $html;
		}
	}
?>