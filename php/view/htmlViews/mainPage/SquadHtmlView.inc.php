<?php
	class SquadHtmlView extends MainPageHtmlView{
		### attributes
		private $squad;

		### methodes

		public function __construct() {

			//init meta-data
			$this->title			= 'Mannschaften - TuS Steinbach Badminton';
			$this->metaDescription 	= 'Alle Informationen zu unsereren Mannschaften';
			$this->metaKeywords 	= array('TuS Steinbach', 'Badminton', 'Ergenisse', 'Tabelle', 'Spieler', 'Termine', 'Statistik');
			$this->activeMenuItem	= 'squads';
		}

		public function setPageData($squad) {
			$this->squad	= $squad;

			//update meta-data
			$name = HtmlView::formatStringToHtml($this->squad->getName());
			$this->title = $name.' - Mannschaften - TuS Steinbach Badminton';
			$this->metaDescription 	= 'Alle Informationen zu unserer Mannschaft '.$name;
			$this->metaKeywords[] = $name;
		}

		protected function getContentAreaHtml() {
			$squadSectionHtml	= $this->getSquadSectionHtml();
			$sidebarSectionHtml	= $this->getSidebarSectionHtml();


			$html = $squadSectionHtml
					.$sidebarSectionHtml;

			return $html;
		}

		private function getSquadSectionHtml() {
			$squadUI = new SquadUI($this->squad);

			$squadSectionHtml = $squadUI->getSquadSectionHtml();

			$html = '<section class="mainContentLeft">'
						.$squadSectionHtml
					.'</section>';
			return $html;
		}

		private function getSidebarSectionHtml() {
			$squadUI = new SquadUI($this->squad);

			$playersSectionHtml 		= $this->getPlayersSectionHtml();
			$alleturniereSectionHtml	= $squadUI->getAlleturniereSectionHtml();

			$html = '<aside class="sidebarRight">'
						.$playersSectionHtml
						.$alleturniereSectionHtml
					.'</aside>';

			return $html;
		}

		private function getPlayersSectionHtml() {
			$malePlayerListHtml		= PlayerUI::getPlayerListHtml($this->squad->getMalePlayers());
			$femalePlayerListHtml	= PlayerUI::getPlayerListHtml($this->squad->getFemalePlayers());

			$html = '<section class="sidebarBox">'
						.'<h3>Spieler</h3>'
						.'<h4>Damen</h4>'
						.$femalePlayerListHtml
						.'<h4>Herren</h4>'
						.$malePlayerListHtml
					.'</section>';

			return $html;
		}

	}
?>