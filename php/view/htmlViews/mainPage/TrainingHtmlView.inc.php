<?php
	class TrainingHtmlView extends MainPageHtmlView{
		### attributes

		### methodes

		public function __construct() {
			//init meta-data
			$this->title			= 'Training - TuS Steinbach Badminton';
			$this->metaDescription 	= 'Informationen zu unserem Training und Trainingszeiten.';
			$this->metaKeywords 	= array('Training', 'Trainingszeiten', 'Badminton', 'TuS Steinbach');
			$this->activeMenuItem	= 'training';
			$this->showSlideShowOverlay = true;
		}

		protected function getContentAreaHtml() {

			$html = $this->getScheduleSectionHtml()
					.$this->getGallerySectionHtml();

			return $html;
		}

		private function getScheduleSectionHtml() {

			$html = '<article class="contentBox contentHalfLeft">'
						.'<h2>Trainingszeiten</h2>'
						.'<table class="verticalTable">'
							.'<tr>'
								.'<th>Minis</th>'
								.'<td>Mittwoch 18:00 bis 19:00 Uhr</td>'
							.'</tr>'
							.'<tr>'
								.'<th rowspan="2">Jugend</th>'
								.'<td>Mittwoch 19:00 bis 20:00 Uhr</td>'
							.'</tr>'
							.'<tr>'
								.'<td>Freitag 19:00 bis 20:00 Uhr</td>'
							.'</tr>'
							.'<tr>'
								.'<th rowspan="2">Senioren</th>'
								.'<td>Freitag 19:00 bis 20:00 Uhr (Training)</td>'
							.'</tr>'
							.'<tr>'
								.'<td>Freitag 20:00 bis 22:00 Uhr (freies Spiel)</td>'
							.'</tr>'
						.'</table>'
						.'<p>'
							.'Wir sind immer auf der Suche nach Verstärkung für unsere Mannschaften!<br />'
							.'Das Training findet in der Altkönighalle (<a href="/kontakt/#anfahrt">Anfahrt</a>) statt.'
						.'</p>'
					.'</article>';

			return $html;
		}

		private function getGallerySectionHtml() {
			$this->showSlideShowOverlay = true;
			$html = '<aside class="contentBox contentHalfRight">'
						.'<h2>Bilder</h2>'
						.'<ul class="galleryThumbs galleryOmega39">'
							.'<li><a href="/images/training/big/training-1.jpg"><img src="/images/training/thumbs/training-1.jpg" class="thumb" /></a></li>'
							.'<li><a href="/images/training/big/training-2.jpg"><img src="/images/training/thumbs/training-2.jpg" class="thumb" /></a></li>'
							.'<li><a href="/images/training/big/training-3.jpg"><img src="/images/training/thumbs/training-3.jpg" class="thumb" /></a></li>'
							.'<li><a href="/images/training/big/training-4.jpg"><img src="/images/training/thumbs/training-4.jpg" class="thumb" /></a></li>'
							.'<li><a href="/images/training/big/training-5.jpg"><img src="/images/training/thumbs/training-5.jpg" class="thumb" /></a></li>'
						.'</ul>'
					.'</aside>';

			return $html;
		}
	}
?>