<?php
	class TournamentAnnouncementHtmlView extends MainPageHtmlView {
		### attributes


		### methodes

		public function __construct() {
			//init meta data
			$this->title 			= 'Ausschreibung - Altkönigturnier 2013 - Badminton TuS Steinbach';
			$this->metaDescription 	= 'Ausschreibung für das nächste 2. Steinbacher Altkönigturnier am 4./5. Mai 2013. Jetzt schnell anmelden! Spielklassen: Bezirksoberliga, A+B+C-Klasse, Hobby';
			$this->metaKeywords 	= array('Altkönigturnier', 'Ausschreibung', 'Badmintonturnier', 'Frankfurt', 'Hobby', 'Turnier', 'Badminton', 'TuS Steinbach');
			$this->activeMenuItem 	= 'tournament';
		}

		protected function getContentAreaHtml() {

			$html = $this->getAnnouncementSectionHtml()
					.$this->getSidebarSectionHtml();

			return $html;
		}

		private function getAnnouncementSectionHtml() {

			$html = '<article class="mainContentLeft contentBox">'
						.'<h2>2. Steinbacher Altkönigturnier</h2>'
						.'<table class="verticalTable">'
							.'<tr>'
								.'<th>Datum</th>'
								.'<td><strong>04./05. Mai 2013</strong></td>'
							.'</tr>'
							.'<tr>'
								.'<th>Veranstalter</th>'
								.'<td>'
									.'<strong>TuS-Steinbach, Badmintonabteilung</strong><br />'
									.'Schirmherr Bürgermeister Dr. Stefan Naas'
								.'</td>'
							.'</tr>'
							.'<tr>'
								.'<th>Austragungsort</th>'
								.'<td>'
									.'<strong>Altkönighalle Steinbach</strong><br />'
									.'<strong>Waldstraße, 61449 Steinbach Ts.</strong><br />'
									.'<a href="/kontakt/#anfahrt">Routenplaner + Karte</a>'
								.'</td>'
							.'</tr>'
							.'<tr>'
								.'<th>Disziplinen</th>'
								.'<td><strong>DE, HE, DD, HD, MIX</strong></td>'
							.'</tr>'
							.'<tr>'
								.'<th>Spielbeginn</th>'
								.'<td>'
									.'<strong>Samstag, 04.05.2013</strong><br />'
									.'HD ab 9:00 Uhr<br />'
									.'DD ab 9:00 Uhr<br />'
									.'MIX ab 14:00 Uhr<br />'
									.'<br />'
									.'<strong>Sonntag, 05.05.2013</strong><br />'
									.'HE ab 10:00 Uhr<br />'
									.'DE ab 12:00 Uhr<br />'
								.'</td>'
							.'</tr>'
							.'<tr>'
								.'<th>Klasseneinteilung</th>'
								.'<td>'
									.'Gruppe 1 (Bezirksoberliga, Bezirksliga A und B)<br />'
									.'Gruppe 2 (Bezirksliga C und HobbySpieler)<br />'
									.'<br />'
									.'Maßgebend ist die Spielsaison 2012/2013<br />'
									.'Bei Doppelpartnern unterschiedlicher Spielklassen gilt der höher spielende Partner!'
								.'</td>'
							.'</tr>'
							.'<tr>'
								.'<th>Austragungsmodus</th>'
								.'<td>Gruppenspiele mit anschließendem KO-System</td>'
							.'</tr>'
							.'<tr>'
								.'<th>Bälle</th>'
								.'<td>'
									.'Naturfederbälle, die von den Teilnehmern gestellt werden müssen.'
								.'</td>'
							.'</tr>'
							.'<tr>'
								.'<th>Anwesenheit</th>'
								.'<td><strong>30 Minuten vor Spielbeginn</strong></td>'
							.'</tr>'
							.'<tr>'
								.'<th>Startgebühren</th>'
								.'<td>'
									.'7,- Euro pro Person und Disziplin<br />'
									.'15,- Euro pro Person für alle 3 Disziplinen'
								.'</td>'
							.'</tr>'
							.'<tr>'
								.'<th>Meldeschluss</th>'
								.'<td>'
									.'<strong>Montag, 29.04.2013</strong><br />'
									.'<strong>Achtung: Meldeplätze sind begrenzt</strong><br />'
									.'<strong>Startplätze werden gemäß dem Meldedatum vergeben.</strong>'
								.'</td>'
							.'</tr>'
							.'<tr>'
								.'<th>Meldungen</th>'
								.'<td><strong>Per E-Mail an <a href="mailto:altkoenigturnier@tus-steinbach-badminton.de">altkoenigturnier@tus-steinbach-badminton.de</a> oder per <a href="/altkoenigturnier/meldungen/">Onlineformular</a></strong></td>'
							.'</tr>'
							.'<tr>'
								.'<th>Auslosung</th>'
								.'<td>30 Minuten vor Spielbeginn</td>'
							.'</tr>'
							.'<tr>'
								.'<th>Turnierleitung</th>'
								.'<td>Mitglieder der Badmintonabteilung TuS Steinbach</td>'
							.'</tr>'
							.'<tr>'
								.'<th>Preise</th>'
								.'<td>Plätze 1-3 erhalten Pokale, Urkunden und Sachpreise</td>'
							.'</tr>'
							.'<tr>'
								.'<th>Spielkleidung</th>'
								.'<td>HBV-Kleiderordnung</td>'
							.'</tr>'
							.'<tr>'
								.'<th>Haftung</th>'
								.'<td>Für Unfälle und Schadensfälle jeglicher Art haftet der Veranstalter nicht.</td>'
							.'</tr>'
							.'<tr>'
								.'<th>Sonstiges</th>'
								.'<td>'
									.'Jeder Teilnehmer kann als Schiedsrichter eingesetzt werden.<br />'
									.'Der Veranstalter behält sich Änderungen jeglicher Art vor.<br />'
									.'Mit der Anmeldung erklären sich die Teilnehmer einverstanden, dass von Ihnen Fotos veröffentlicht werden dürfen.'
								.'</td>'
							.'</tr>'
							.'<tr>'
								.'<th>Racket-Company</th>'
								.'<td>Die <a href="http://racket-company.de/">Racket-Company</a> ist wieder mit tollen Angeboten und Besaitungsservice vor Ort.</td>'
							.'</tr>'
							.'<tr>'
								.'<th>Verpflegung</th>'
								.'<td>Wir haben selbstverständlich für euer leibliches Wohl gesorgt.</td>'
							.'</tr>'
						.'</table>'
					.'</article>';

			return $html;
		}

		private function getSidebarSectionHtml() {
			$html = '<aside class="sidebarRight">'
						.'<section class="sidebarBox">'
							.'<h3>Informationen</h3>'
							.'<a href="/turniere/ausschreibungen/2013/05/04/2-steinbacher-altkoenigturnier.pdf" class="button">Download PDF</a>'
							.'<a href="/altkoenigturnier/meldeliste/" class="button">Meldeliste</a>'
						.'</section>'
						.'<section class="sidebarBox">'
							.'<h3>Noch Fragen?</h2>'
							.'<p>Dann schreiben Sie uns unter '.HtmlView::secureEMailLink('altkoenigturnier@tus-steinbach-badminton.de').'</p>'
						.'</section>'
					.'</aside>';

			return $html;
		}
	}
?>