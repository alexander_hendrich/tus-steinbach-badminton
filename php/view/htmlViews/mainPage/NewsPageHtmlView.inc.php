<?php
	class NewsPageHtmlView extends MainPageHtmlView{
		### attributes
		private $newsEntries;
		private $pageNum;
		private $maxPageNum;

		### methodes

		public function __construct() {
			//init meta-data
			$this->title			= 'News - TuS Steinbach Badminton';
			$this->metaDescription 	= 'Willkommen auf der Website der Badmintonabteilung des TuS Steinbach. Hier erfahren Sie alles über unsere Mannschaften, Training, Turniere und das Altkönigturnier!';
			$this->metaKeywords 	= array('TuS Steinbach', 'Badminton', 'News', 'Nachrichten', 'Spielberichte', 'Turnierberichte');
			$this->activeMenuItem	= 'news';
		}

		public function setPageData($newsEntries, $pageNum, $maxPageNum) {
			$this->newsEntries	= $newsEntries;
			$this->pageNum		= $pageNum;
			$this->maxPageNum	= $maxPageNum;
		}

		protected function getContentAreaHtml() {
			$html = $this->getPreviewsSectionHtml()
					.$this->getSidebarSectionHtml();

			return $html;
		}

		private function getPreviewsSectionHtml() {
			$newsEntryPreviewsHtml = '';
			foreach($this->newsEntries as $newsEntry) {
				$newsEntryUI = new NewsEntryUI($newsEntry);
				$newsEntryPreviewsHtml .= $newsEntryUI->getPreviewSectionHtml();
			}

			$paginationHtml = $this->getPaginationHtml();

			$html = '<section class="mainContentLeft">'
					.$newsEntryPreviewsHtml
					.$paginationHtml
					.'</section>';

			return $html;
		}

		private function getSidebarSectionHtml() {
			$html = '<aside class="sidebarRight">';

			if($this->pageNum == 1) {
				$html .= '<section class="sidebarBox">'
							.'<h3>Willkommen</h3>'
							.'<p>Willkommen auf der Website der Badmintonabteilung des TuS Steinbach. Hier erfahren Sie alles über unsere <a href="/mannschaften/senioren/">Mannschaften</a>, <a href="/training/">Training</a>, <a href="/turniere/">Turniere</a> und das <a href="/altkoenigturnier/ausschreibung/">Altkönigturnier</a>!</p>'
						.'</section>';
			} else {
				$html .= '';
			}

			$html .= 	'<section class="sidebarBox">'
							.'<h3>Altkönigturnier</h3>'
							.'<p>Am 04./05. Mai 2013 findet die nächste Ausgabe des Altkönigturniers statt. Jetzt schnell anmelden!</p>'
							.'<a href="/altkoenigturnier/ausschreibung/" class="button">Ausschreibung</a>'
							.'<a href="/altkoenigturnier/meldeliste/" class="button">Anmeldung</a>'
						.'</section>'
						/*.'<section class="sidebarBox">'
							.'<h3>Sponsoren</h2>'
							.'<a href="http://ds-sport.de/"><img src="/images/sponsors/ds_sport.png" alt="ds-sport" /></a>'
							.'<a href="http://www.oliver-sport.com/"><img src="/images/sponsors/oliver.png" alt="oliver" /></a>'
						.'</section>'*/
					.'</aside>';

			return $html;
		}

		private function getPaginationHtml() {

			$paginationItemsHtml = '';
			for($i=1; $i<=$this->maxPageNum; $i++) {
				$paginationItemsHtml .= '<li class="'.(($i == $this->pageNum) ? 'active' : '').'"><a href="/news/seite/'.$i.'/">'.$i.'</a></li>';
			}


			$html = '<section class="contentBox pagination">'
						.'<span>Seite(n):</span>'
						.'<ul>'
							.$paginationItemsHtml
						.'</ul>'
					.'</section>';


			return $html;

		}

	}
?>