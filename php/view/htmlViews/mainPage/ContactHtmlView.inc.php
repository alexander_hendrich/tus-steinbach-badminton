<?php
	class ContactHtmlView extends MainPageHtmlView{
		### attributes
		private $sendContactFormDefaultValues;
		private $feedbackPresenter;

		### methodes

		public function __construct($feedbackPresenter) {
			$this->feedbackPresenter			= $feedbackPresenter;

			//init meta-data
			$this->title 			= 'Kontakt - Badminton TuS Steinbach';
			$this->metaDescription 	= 'Anfahrt zu unserer Halle und Kontaktdaten.';
			$this->metaKeywords 	= array('TuS Steinbach', 'Badminton', 'Kontak', 'Anfahrt', 'Karte', 'Kontaktromular');
			$this->activeMenuItem 	= 'contact';
		}

		public function setPageData($sendContactFormDefaultValues) {
			$this->sendContactFormDefaultValues = $sendContactFormDefaultValues;
		}

		protected function getContentAreaHtml() {

			$html = $this->getContactFormSectionHtml()
					.$this->getContactDataSectionHtml()
					.$this->getApproachSectionHtml()
					.$this->getRoutePlannerSectionHtml();

			return $html;
		}

		private function getContactFormSectionHtml() {
			$receiver	= $this->sendContactFormDefaultValues['receiver'];
			$name		= HtmlView::formatStringToHtml($this->sendContactFormDefaultValues['name']);
			$email		= HtmlView::formatStringToHtml($this->sendContactFormDefaultValues['email']);
			$subject	= HtmlView::formatStringToHtml($this->sendContactFormDefaultValues['subject']);
			$message	= HtmlView::formatStringToHtml($this->sendContactFormDefaultValues['message']);

			$html = '<section class="mainContentLeft contentBox">'
						.'<h2>Kontaktformular</h2>'
						.'<form method="post" action="#contactForm" id="sendContactForm">'
							.$this->feedbackPresenter->getFeedbackBigHtml('sendContactForm', 'general')
							.'<table class="verticalTable">'
								.'<tr>'
									.'<th><label for="sendContactFormReceiver">Empfänger</label></th>'
									.'<td>'
										.'<select name="sendContactFormReceiver" id="sendContactFormReceiver">'
											.'<option '.(($receiver === 'abteilungsleitung') ? 'selected="selected"' : '').' value="abteilungsleitung">Heike Schwab (abteilungsleitung@tus-steinbach-badminton.de)</option>'
											.'<option '.(($receiver === 'jugendmannschaft') ? 'selected="selected"' : '').' value="jugendmannschaft">Wolfgang Kaul (jugendmannschaft@tus-steinbach-badminton.de)</option>'
											.'<option '.(($receiver === 'altkoenigturnier') ? 'selected="selected"' : '').' value="altkoenigturnier">Daniel Scheffler (altkoenigturnier@tus-steinbach-badminton.de)</option>'
											.'<option '.(($receiver === 'webmaster') ? 'selected="selected"' : '').' value="webmaster">Alexander Hendrich (webmaster@tus-steinbach-badminton.de)</option>'
										.'</select>'
									.$this->feedbackPresenter->getFeedbackSmallHtml('sendContactForm', 'receiver')
									.'</td>'
								.'</tr>'
								.'<tr>'
									.'<th><label for="sendContactFormName">Name</label></th>'
									.'<td>'
										.'<input type="text" name="sendContactFormName" id="sendContactFormName" placeholder="Ihr Name" value="'.$name.'" />'
									.$this->feedbackPresenter->getFeedbackSmallHtml('sendContactForm', 'name')
									.'</td>'
								.'</tr>'
								.'<tr>'
									.'<th><label for="sendContactFormEMail">E-Mail</label></th>'
									.'<td>'
										.'<input type="email" name="sendContactFormEMail" id="sendContactFormEMail" placeholder="Ihre E-Mail-Adresse" value="'.$email.'" />'
									.$this->feedbackPresenter->getFeedbackSmallHtml('sendContactForm', 'email')
									.'</td>'
								.'</tr>'
								.'<tr>'
									.'<th><label for="sendContactFormSubject">Betreff</label></th>'
									.'<td>'
										.'<input type="text" name="sendContactFormSubject" id="sendContactFormSubject" placeholder="Betreff" value="'.$subject.'" />'
									.$this->feedbackPresenter->getFeedbackSmallHtml('sendContactForm', 'subject')
									.'</td>'
								.'</tr>'
								.'<tr>'
									.'<th><label for="sendContactFormMessage">Nachricht</label></th>'
									.'<td>'
										.'<textarea name="sendContactFormMessage" id="sendContactFormMessage">'.$message.'</textarea>'
									.$this->feedbackPresenter->getFeedbackSmallHtml('sendContactForm', 'message')
									.'</td>'
								.'</tr>'
								.'<tr>'
									.'<td colspan="2">'
										.'<input type="submit" name="sendContactForm" value="E-Mail verschicken" class="button right" />'
										.HtmlView::getSpamPreventionHtml()
									.'</td>'
								.'</tr>'
							.'</table>'
						.'</form>'
					.'</section>';

			return $html;
		}

		private function getContactDataSectionHtml() {

			$html = '<aside class="sidebarRight sidebarBox ">'
						.'<h3>Kontaktdaten</h3>'
						.'<ul class="contactInfo">'
							.'<li>'
								.'<span class="name">Heike Schwab</span>'
								.'<span class="responsibility">Abteilungsleitung</span>'
								.HtmlView::secureEMailLink('abteilungsleitung@tus-steinbach-badminton.de')
							.'</li>'
							.'<li>'
								.'<span class="name">Wolfgang Kaul</span>'
								.'<span class="responsibility">Jugendmannschaft, Vereinsgründer</span>'
								.HtmlView::secureEMailLink('jugendmannschaft@tus-steinbach-badminton.de')
							.'</li>'
							.'<li>'
								.'<span class="name">Daniel Scheffler</span>'
								.'<span class="responsibility">Altkönigturnier</span>'
								.HtmlView::secureEMailLink('altkoenigturnier@tus-steinbach-badminton.de')
							.'</li>'
							.'<li>'
								.'<span class="name">Alexander Hendrich</span>'
								.'<span class="responsibility">Website, Turniere</span>'
								.HtmlView::secureEMailLink('webmaster@tus-steinbach-badminton.de')
							.'</li>'
						.'</ul>'
					.'</aside>';

			return $html;
		}

		private function getApproachSectionHtml() {

			$html = '<section class="mainContentLeft contentBox" id="anfahrt">'
						.'<h2>Anfahrt Altkönighalle</h2>'
						.'<iframe width="600" height="400" class="shadowed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.de/maps?f=d&amp;source=s_d&amp;saddr=Waldstra%C3%9Fe&amp;daddr=&amp;hl=de&amp;geocode=FRWh_QId-LKCAA&amp;aq=&amp;sll=50.176293,8.565468&amp;sspn=0.002078,0.004823&amp;t=h&amp;dirflg=w&amp;mra=ls&amp;ie=UTF8&amp;ll=50.176431,8.566332&amp;spn=0.010994,0.025706&amp;z=15&amp;output=embed"></iframe>'
					.'</section>';

			return $html;
		}

		private function getRoutePlannerSectionHtml() {

			$html = '<aside class="sidebarRight sidebarBox">'
						.'<h3>Routenplaner</h3>'
						.'<form method="get" action="http://maps.google.com/maps" class="sidebarForm">'
							.'<label for="saddr">Start-Adresse</label>'
							.'<input type="text" name="saddr" id="saddr" />'
							.'<label for="daddr">Ziel-Adresse</label>'
							.'<input type="text" name="daddr" id="daddr" value="Waldstraße, 61449 Steinbach" readonly="readonly" />'
							.'<input type="submit" value="Route berechnen" class="button right" />'
						.'</form>'
					.'</aside>';

			return $html;
		}


	}
?>