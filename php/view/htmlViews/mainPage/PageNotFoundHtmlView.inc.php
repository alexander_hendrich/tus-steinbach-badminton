<?php
	class PageNotFoundHtmlView extends MainPageHtmlView{
		### attributes
		
		### methodes
		
		public function __construct() {
			//init meta-data
			$this->title 			= '404 - Seite nicht gefunden - TuS Steinbach badminton';
			$this->metaDescription 	= 'Fehler 404, die Seite wurde nicht gefunden!';
			$this->metaKeywords 	= array('404', 'Fehler', 'Seite nicht gefunden');
			$this->activeMenuItem 	= '';
		}
		
		protected function getContentAreaHtml() {
			$html = '<section class="mainContentLeft contentBox">'
						.'<h2>Fehler 404 - Seite nicht gefunden!</h2>'
						.'<p>Es ist ein Fehler aufgetreten! Die von Ihnen angeforderte Seite wurde nicht gefunden. Bei Fragen kontaktieren Sie bitte den Webmaster ('.HtmlView::secureEMailLink('webmaster@tus-steinbach-badminton.de').').</p>'
					.'</section>';
					
			return $html;
		}		
	}
?>