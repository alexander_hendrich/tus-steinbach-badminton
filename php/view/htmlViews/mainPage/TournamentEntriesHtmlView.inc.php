<?php
	class TournamentEntriesHtmlView extends MainPageHtmlView{
		### attributes
		private $disciplines;
		private $addEntryDefaultValues;
		private $feedbackPresenter;

		### methodes

		public function __construct($feedbackPresenter) {
			$this->feedbackPresenter	= $feedbackPresenter;

			//init meta data
			$this->title 			= 'Meldeliste - Altkönigturnier 2013 - Badminton TuS Steinbach';
			$this->metaDescription 	= 'Meldeliste für das 2. Steinbacher Altkönigturnier 2013! Jetzt schnell anmelden!';
			$this->metaKeywords 	= array('Anmeldung', 'Meldeliste', 'Altkönigturnier', 'Ausschreibung', 'Badmintonturnier', 'Frankfurt', 'Hobby', 'Turnier', 'Badminton', 'TuS Steinbach');
			$this->activeMenuItem 	= 'tournament';
		}

		public function setPageData($disciplines, $addEntryDefaultValues) {
			$this->disciplines 				= $disciplines;
			$this->addEntryDefaultValues	= $addEntryDefaultValues;
		}

		protected function getContentAreaHtml() {

			$html = $this->getEntriesSectionHtml()
					.$this->getSidebarSectionHtml();

			return $html;
		}

		private function getEntriesSectionHtml() {
			$disciplineTablesHtml 			= DisciplineUI::getDisciplineTablesHtml($this->disciplines);
			$disciplineSelectOptionsHtml	= DisciplineUI::getSelectOptions($this->disciplines);

			$html = '<section class="mainContentLeft contentBox">'
						.'<h2>Meldeliste - 2. Steinbacher Altkönigturnier</h2>'
						.'<select id="disciplineTableChooser">'
							.$disciplineSelectOptionsHtml
						.'</select>'
						.'<p>Bitte wählen Sie die gewünschte Disziplin aus:</p>'
						.$disciplineTablesHtml
					.'</section>';

			return $html;
		}

		private function getSidebarSectionHtml() {
			$email			= HtmlView::formatStringToHtml($this->addEntryDefaultValues['email']);
			$disciplineID	= HtmlView::formatStringToHtml($this->addEntryDefaultValues['disciplineID']);
			$player1Name	= HtmlView::formatStringToHtml($this->addEntryDefaultValues['player1Name']);
			$player2Name	= HtmlView::formatStringToHtml($this->addEntryDefaultValues['player2Name']);
			$player1Squad	= HtmlView::formatStringToHtml($this->addEntryDefaultValues['player1Squad']);
			$player2Squad	= HtmlView::formatStringToHtml($this->addEntryDefaultValues['player2Squad']);

			$disciplineSelectOptionsHtml = DisciplineUI::getSelectOptions($this->disciplines, $disciplineID);

			$html = '<aside class="sidebarRight">'
						.'<section class="sidebarBox">'
							.'<h3>Anmeldung</h3>'
							.'<form method="POST" action="#addEntry" class="sidebarForm" id="addEntry">'
							.$this->feedbackPresenter->getFeedbackBigHtml('addEntry', 'general')

								.'<label for="addEntryEMail">E-Mail <span class="inputInfo">(wird nicht veröffentlicht)</span></label>'
								.'<input type="email" name="addEntryEMail" id="addEntryEMail" placeholder="E-Mail" value="'.$email.'" />'
								.$this->feedbackPresenter->getFeedbackSmallHtml('addEntry', 'email')

								.'<label for="addEntryDisciplineID">Disziplin<span class="inputInfo">(Gruppe 1: A+B)<br />(Gruppe 2: C+Hobby)</span></label>'
								.'<select name="addEntryDisciplineID" id="addEntryDisciplineID">'
									.$disciplineSelectOptionsHtml
								.'</select>'
								.$this->feedbackPresenter->getFeedbackSmallHtml('addEntry', 'disciplineID')

								.'<label for="addEntryPlayer1Name" id="addEntryPlayer1NameLabel">Spieler 1 Name:</label>'
								.'<input type="text" name="addEntryPlayer1Name" id="addEntryPlayer1Name" placeholder="Name"  value="'.$player1Name.'" />'
								.$this->feedbackPresenter->getFeedbackSmallHtml('addEntry', 'player1Name')

								.'<label for="addEntryPlayer2Name" id="addEntryPlayer2NameLabel">Spieler 2 Name:</label>'
								.'<input type="text" name="addEntryPlayer2Name" id="addEntryPlayer2Name" placeholder="Name" value="'.$player2Name.'" />'
								.$this->feedbackPresenter->getFeedbackSmallHtml('addEntry', 'player2Name')

								.'<label for="addEntryPlayer1Squad" id="addEntryPlayer1SquadLabel">Spieler 1 Verein:</label>'
								.'<input type="text" name="addEntryPlayer1Squad" id="addEntryPlayer1Squad" placeholder="Verein" value="'.$player1Squad.'" />'
								.$this->feedbackPresenter->getFeedbackSmallHtml('addEntry', 'player1Squad')

								.'<label for="addEntryPlayer2Squad" id="addEntryPlayer2SquadLabel">Spieler 2 Verein:</label>'
								.'<input type="text" name="addEntryPlayer2Squad" id="addEntryPlayer2Squad" placeholder="Verein" value="'.$player2Squad.'" />'
								.$this->feedbackPresenter->getFeedbackSmallHtml('addEntry', 'player2Squad')

								.'<p>Die Anmeldung wird erst durch die Turnierleitung geprüft, Sie erhalten anschließend eine Bestätigung per E-Mail. Die Anmeldung ist verbindlich!</p>'
								.'<input type="submit" name="addEntry" value="Anmelden" class="button" />'

								.HtmlView::getSpamPreventionHtml()
							.'</form>'
						.'</section>'
					.'</aside>';

			return $html;
		}

	}
?>