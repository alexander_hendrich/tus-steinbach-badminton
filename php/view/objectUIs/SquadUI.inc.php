<?php
	class SquadUI {
		### attributes
		private $squad;
		
		### methodes
		
		public function __construct($squad) {
			$this->squad = $squad;
		}
		
		public function getSquadSectionHtml() {
			$squad = $this->squad;
			
			$key	= $squad->getKey();
			$name	= HtmlView::formatStringToHtml($squad->getName());
			$report	= HtmlView::formatTextToHtml($squad->getReport());
			
			$html = '<article class="contentBox squad">'
						.'<h2>'.$name.'</h2>'
						.'<img src="/images/squads/'.$key.'.jpg" class="squadPortrait" alt="'.$key.'" />'
						.'<h3>Saisonbericht</h3>'
						.'<p>'.$report.'</p>'
					.'</article>';
					
			return $html;
		}
		
		public function getAlleturniereSectionHtml() {
			$squad = $this->squad;
			
			$alleturniereTournamentID	= $squad->getAlleturniereTournamentID();
			$alleturniereTeamID			= $squad->getAlleturniereTeamID();
			$alleturniereDrawID			= $squad->getAlleturniereDrawID();
			
			
			$html = '<section class="sidebarBox">'
						.'<h3>Alleturniere</h3>'
						.'<p>mehr Informationen zu unserer Mannschaft finden sie auf <a href="http://www.alleturniere.de/">www.alleturniere.de</a></p>'
						.'<a class="button" href="http://alleturniere.de/sport/team.aspx?id='.$alleturniereTournamentID.'&team='.$alleturniereTeamID.'">Übersicht</a>'
						.'<a class="button" href="http://alleturniere.de/sport/draw.aspx?id='.$alleturniereTournamentID.'&draw='.$alleturniereDrawID.'">Tabelle</a>'
						.'<a class="button" href="http://alleturniere.de/sport/teammatches.aspx?id='.$alleturniereTournamentID.'&tid='.$alleturniereTeamID.'">Ergebnisse & Termine</a>'
						.'<a class="button" href="http://alleturniere.de/sport/teamrankingplayers.aspx?id='.$alleturniereTournamentID.'&tid='.$alleturniereTeamID.'">Spieler</a>'
						.'<a class="button" href="http://alleturniere.de/sport/teamstats.aspx?id='.$alleturniereTournamentID.'&tid='.$alleturniereTeamID.'">Statistik</a>'
						.'</ul>'
					.'</section>';
					
			return $html;
		}
	}
?>