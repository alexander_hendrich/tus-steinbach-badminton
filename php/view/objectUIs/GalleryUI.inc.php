<?php
	class GalleryUI {
		### attributes
		private $gallery;
		
		### methodes
		
		public function __construct($gallery) {
			$this->gallery = $gallery;
		}
		
		public function getCoverImageHtml() {
			$coverImage 	= $this->gallery->getCoverImage();
			
			$coverImageUI = new GalleryImageUI($coverImage);
			
			return $coverImageUI->getCoverImageHtml();
		}
		
		public function getThumbListHtml() {
			$galleryImages = $this->gallery->getImages();
			
			return GalleryImageUI::getThumbListHtml($galleryImages);
		}
		
	}

?>