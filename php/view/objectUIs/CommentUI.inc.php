<?php
	class CommentUI {
		### attributes
		private $comment;
		
		### methodes
		
		public function __construct($comment) {
			$this->comment = $comment;
		}
		
		public function getCommentListItemHtml() {
			$comment = $this->comment;
			
			$authorName	= HtmlView::formatStringToHtml($comment->getAuthorName());
			$date		= date('d.m.Y', $comment->getDateTime());
			$time		= date('H:i', $comment->getDateTime());
			$text		= HtmlView::formatTextToHtml($comment->getText());
			
			$html = '<li class="comment">'
						.'<p>'.$text.'</p>'
						.'<span class="info">von '.$authorName.' am '.$date.' um '.$time.' Uhr</span>'
					.'</li>';
			
			return $html;
		}
		
		public static function getCommentListHtml($comments) {
			$commentListItemsHtml = '';
			foreach($comments as $comment) {
				$commentUI = new CommentUI($comment);
				$commentListItemsHtml .= $commentUI->getCommentListItemHtml();
			}
			
			$html = '<ul class="comments">'
						.$commentListItemsHtml
					.'</ul>';
					
			return $html;
		}
		
		
	}

?>