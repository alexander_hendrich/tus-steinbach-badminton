<?php
	class EntryUI {
		### attributes
		private $entry;

		### methodes

		public function __construct($entry) {
			$this->entry = $entry;
		}

		public function getEntryRowHtml($index, $isDouble, $waiting) {
			$entry = $this->entry;

			$player1Name	= HtmlView::formatStringToHtml($entry->getPlayer1Name());
			$player1Squad	= HtmlView::formatStringToHtml($entry->getPlayer1Squad());

			if($isDouble) {
				$player2Name	= HtmlView::formatStringToHtml($entry->getPlayer2Name());
				$player2Squad	= HtmlView::formatStringToHtml($entry->getPlayer2Squad());

				$html = '<tr '.($waiting ? 'class="waiting"' : '').'>'
							.'<td>'.($waiting ? 'W' : '').$index.'</td>'
							.'<td>'.$player1Name.'<br />'.$player2Name.'</td>'
							.'<td>'.$player1Squad.'<br />'.$player2Squad.'</td>'
						.'</tr>';
			} else {
				$html = '<tr '.($waiting ? 'class="waiting"' : '').'>'
							.'<td>'.($waiting ? 'W' : '').$index.'</td>'
							.'<td>'.$player1Name.'</td>'
							.'<td>'.$player1Squad.'</td>'
						.'</tr>';
			}

			return $html;
		}

		public function getAdminEntriesTableRowHtml($index, $accept, $waiting) {
			$entry 		= $this->entry;
			$discipline	= $entry->getDiscipline();

			$id		= $entry->getID();
			$name	= HtmlView::formatStringToHtml($entry->getPlayer1Name());
			$squad	= HtmlView::formatStringToHtml($entry->getPlayer1Squad());

			if($discipline->isDouble()) {
				$name	.= '<br />'.HtmlView::formatStringToHtml($entry->getPlayer2Name());
				$squad	.= '<br />'.HtmlView::formatStringToHtml($entry->getPlayer2Squad());
			}


			$acceptButton 		= '';
			$waitingButton	= '';
			if($accept) {
				$acceptButton = '<input type="submit" name="entrySetStateAccepted" value="Annehmen" />';
			}
			if($waiting) {
				$waitingButton = '<input type="submit" name="entrySetStateWaiting" value="Auf die Warteliste" />';
			}

			$actions =	'<form action="" method="post">'
							.'<input type="hidden" name="entryID" value="'.$id.'" />'
							.$acceptButton
							.$waitingButton
						.'</form>'
						.'<a href="/admin/entries/edit/'.$id.'/">bearbeiten</a> '
						.'<a href="/admin/entries/delete/'.$id.'/">löschen</a>';

			$html = '<tr>'
						.'<td>'.$index.'</td>'
						.'<td>'.$name.'</td>'
						.'<td>'.$squad.'</td>'
						.'<td>'.$actions.'</td>'
					.'</tr>';

			return $html;
		}

		public static function getEntryRowsHtml($entries, $isDouble, $waiting) {
			$index = 1;

			if($waiting) {
				$entryRowsHtml = '<tr class="waiting"><th colspan="3">WARTELISTE</th></tr>';
			} else {
				$entryRowsHtml = '';
			}

			foreach($entries as $entry) {
				$entryUI = new EntryUI($entry);
				$entryRowsHtml .= $entryUI->getEntryRowHtml($index, $isDouble, $waiting);
				$index++;
			}

			return $entryRowsHtml;
		}

		public static function getAdminEntriesTableHtml($entries, $accept=false, $waiting=false) {
			$tableRowsHtml = '';
			$index = 0;
			foreach($entries as $entry) {
				$index++;
				$entryUI = new EntryUI($entry);
				$tableRowsHtml .= $entryUI->getAdminEntriesTableRowHtml($index, $accept, $waiting);
			}

			$html = '<table class="horizontalTable">'
						.'<tr>'
							.'<th>#</th>'
							.'<th>Name</th>'
							.'<th>Verein</th>'
							.'<th>Aktionen</th>'
						.'</tr>'
						.$tableRowsHtml
					.'</table>';

			return $html;
		}


	}

?>