<?php
	class TournamentUI {
		### attributes
		private $tournament;
		
		### methodes
		
		public function __construct($tournament) {
			$this->tournament = $tournament;
		}
		
		public function getTournamentsListItemHtml() {
			$tournament = $this->tournament;
			
			$monthsGer = array(
				'1'		=>	'Januar',
				'2'		=>	'Februar',
				'3'		=>	'März',
				'4'		=>	'April',
				'5'		=>	'Mai',
				'6'		=>	'Juni',
				'7'		=>	'Juli',
				'8'		=>	'August',
				'9'		=>	'September',
				'10'	=>	'Oktober',
				'11'	=>	'November',
				'12'	=>	'Dezember'
			);
			
			//name
			$name = HtmlView::formatStringToHtml($tournament->getName());
			
			//date
			$dateStart = $tournament->getDateStart();
			$dayStart	= date('j', $dateStart);
			$monthStart	= date('n', $dateStart);
			$yearStart	= date('Y', $dateStart);
			
			
			if($tournament->isMultipleDays()) {
				$dateEnd	= $tournament->getDateEnd();
				$dayEnd		= date('j', $dateEnd);
				$monthEnd	= date('n', $dateEnd);
				$yearEnd	= date('Y', $dateEnd);
				
				if($monthStart === $monthEnd) {
					$date = $dayStart.'.-'.$dayEnd.'. '.$monthsGer[$monthStart].' '.$yearStart;
				} else {
					$date = $dayStart.'. '.$monthsGer[$monthStart].' - '.$dayEnd.'. '.$monthsGer[$monthEnd].' '.$yearEnd;
				}
			} else {
				$date = $dayStart.'. '.$monthsGer[$monthStart].' '.$yearStart;
			}
			
			//deadline
			$dateDeadline = $tournament->getDateDeadline();
			$dayDeadline 	= date('j', $dateDeadline);
			$monthDeadline 	= date('n', $dateDeadline);
			$yearDeadline 	= date('Y', $dateDeadline);
			$deadline = $dayDeadline.'. '.$monthsGer[$monthDeadline].' '.$yearDeadline;
			
			//location
			$location = HtmlView::formatStringToHtml($tournament->getLocation());
			
			//disciplines
			$disciplines = join(', ', $tournament->getDisciplines());
			
			//leagues
			$leagues = join(', ', $tournament->getLeagues());	
			
			//websiteLink
			if($tournament->hasWebsiteLink()) {
				$link = $tournament->getWebsiteLink();
				$websiteLink = '<a href="'.$link.'">'.$name.'</a>';
			} else {
				$websiteLink = '-';
			}
			
			//announcementLink
			$key	= $tournament->getKey();
			$year 	= date('Y', $dateStart);
			$month 	= date('m', $dateStart);
			$day 	= date('d', $dateStart);
			
			$announcementLink = '<a href="/turniere/ausschreibungen/'.$year.'/'.$month.'/'.$day.'/'.$key.'.pdf">'.$name.'</a>';
			
			$html = '<li>'
						.'<table class="tournamentTable">'
							.'<colgroup>'
								.'<col width="20%" />'
								.'<col width="30%" />'
								.'<col width="20%" />'
								.'<col width="30%" />'
							.'</colgroup>'
							.'<thead>'
								.'<tr>'
									.'<td>'.$date.'</td>'
									.'<th colspan="2">'.$name.'</th>'
									.'<td><a class="details" href="#">Details einblenden</a></td>'
								.'</tr>'
							.'</thead>'
							.'<tbody>'
								.'<tr>'
									.'<th>Meldeschluss</th>'
									.'<td>'.$deadline.'</td>'
									.'<th>Ort</th>'
									.'<td>'.$location.'</td>'
								.'</tr>'
								.'<tr>'
									.'<th>Disziplinen</th>'
									.'<td>'.$disciplines.'</td>'
									.'<th>Ligen</th>'
									.'<td>'.$leagues.'</td>'
								.'</tr>'
								.'<tr>'
									.'<th>Website</th>'
									.'<td>'.$websiteLink.'</td>'
									.'<th>Ausschreibung</th>'
									.'<td>'.$announcementLink.'</td>'
								.'</tr>'
							.'</tbody>'
						.'</table>'
					.'</li>';
					
			return $html;
		}
		
		public static function getTournamentsListHtml($tournaments) {
			$tournamentsListItemsHtml = '';
			foreach($tournaments as $tournament) {
				$tournamentUI = new TournamentUI($tournament);
				$tournamentsListItemsHtml .= $tournamentUI->getTournamentsListItemHtml();
			}
						
			$html = '<ul class="tournamentList">'
						.$tournamentsListItemsHtml
					.'</ul>';
					
			return $html;
		}
		
	}
?>