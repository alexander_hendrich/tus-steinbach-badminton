<?php
	class PlayerUI {
		### attributes
		private $player;
		
		### methodes
		
		public function __construct($player) {
			$this->player = $player;
		}
		
		public function getPlayerListItemHtml() {
			$player = $this->player;
			$squad	= $player->getSquad();
			
			$name						= HtmlView::formatStringToHtml($player->getName());
			$rank						= $player->getRank();
			$alleturniereTournamentID	= $squad->getAlleturniereTournamentID();
			$alleturnierePlayerID		= $player->getAlleturnierePlayerID();
			
			$html = '<li><a href="http://alleturniere.de/sport/player.aspx?id='.$alleturniereTournamentID.'&player='.$alleturnierePlayerID.'">'.$rank.'. '.$name.'</a></li>';
			
			return $html;
		}
		
		public static function getPlayerListHtml($players) {		
			$playerListItemsHtml = '';
			foreach($players as $player) {
				$playerUI = new PlayerUI($player);
				$playerListItemsHtml .= $playerUI->getPlayerListItemHtml();
			}
			
			$html = '<ul class="sidebarList">'
						.$playerListItemsHtml
					.'</ul>';
					
			return $html;
		}
		
		
	}

?>