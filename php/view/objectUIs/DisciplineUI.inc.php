<?php
	class DisciplineUI {
		### attributes
		private $discipline;

		### methodes

		public function __construct($discipline) {
			$this->discipline = $discipline;
		}

		public function getDisciplineSelectOptionHtml($selected) {
			$discipline = $this->discipline;

			$id					= $discipline->getID();
			$group				= $discipline->getGroup();
			$type				= $discipline->getType();
			$name				= HtmlView::formatStringToHtml('Gruppe '.$group.' - '.$type);
			$numEntriesAccepted	= $discipline->getNumEntriesAccepted();
			$numMaxEntries		= $discipline->getNumMaxEntries();
			$numEntriesWaiting	= $discipline->getNumEntriesWaiting();
			$selected			= ($selected) ? 'selected="selected"' : '';

			$html = '<option value="'.$id.'" '.$selected.'>'.$name.' '.$numEntriesAccepted.'/'.$numMaxEntries.' ('.$numEntriesWaiting.')</option>';

			return $html;
		}

		public function getDisciplineTableHtml() {
			$discipline = $this->discipline;

			$id = $discipline->getID();

			$isDouble			= $discipline->isDouble();
			$numEntriesAccepted	= $discipline->getNumEntriesAccepted();
			$numEntriesWaiting	= $discipline->getNumEntriesWaiting();
			$numMaxEntries		= $discipline->getNumMaxEntries();
			$entriesAccepted	= $discipline->getEntriesAccepted();
			$entriesWaiting		= $discipline->getEntriesWaiting();

			$entriesAcceptedRowsHtml = EntryUI::getEntryRowsHtml($entriesAccepted, $isDouble, false);

			$emptyRowsHtml = '';
			for($i=$numEntriesAccepted+1; $i<=$numMaxEntries; $i++) {
				$emptyRowsHtml .= '<tr><td>'.$i.'</td><td></td><td></td></tr>';
			}

			if($numEntriesWaiting > 0) {
				$entriesWaitingRowsHtml = EntryUI::getEntryRowsHtml($entriesWaiting, $isDouble, true);
			} else {
				$entriesWaitingRowsHtml = '';
			}

			$html = '<table class="horizontalTable disciplineTable" id="disciplineTable'.$id.'">'
						.'<colgroup>'
							.'<col width="6%" />'
							.'<col width="47%" />'
							.'<col width="47%" />'
						.'</colgroup>'
						.'<tr>'
							.'<th>#</th>'
							.'<th>Name</th>'
							.'<th>Verein</th>'
						.'</tr>'
						.$entriesAcceptedRowsHtml
						.$emptyRowsHtml
						.$entriesWaitingRowsHtml
					.'</table>';

			return $html;
		}

		public function getAdminNavigationListItemHtml() {
			$discipline = $this->discipline;

			$id					= $discipline->getID();
			$group				= $discipline->getGroup();
			$type				= $discipline->getType();
			$name				= HtmlView::formatStringToHtml('Gruppe '.$group.' - '.$type);
			$numEntriesPending	= $discipline->getNumEntriesPending();

			$html = '<li><a href="/admin/disciplines/'.$id.'/">'.$name.' ('.$numEntriesPending.')</a></li>';

			return $html;
		}

		public static function getDisciplineTablesHtml($disciplines) {
			$disciplineTablesHtml = '';
			foreach($disciplines as $discipline) {
				$disciplineUI = new DisciplineUI($discipline);
				$disciplineTablesHtml	 		.= $disciplineUI->getDisciplineTableHtml();
			}

			return $disciplineTablesHtml;
		}

		public static function getSelectOptions($disciplines, $selectedDisciplineID=null) {
			$disciplineSelectOptionsHtml = '';
			foreach($disciplines as $discipline) {
				$disciplineUI = new DisciplineUI($discipline);
				$selected = ($selectedDisciplineID == $discipline->getID());
				$disciplineSelectOptionsHtml .= $disciplineUI->getDisciplineSelectOptionHtml($selected);
			}

			return $disciplineSelectOptionsHtml;
		}

		public static function getAdminNavigationListHtml($disciplines) {
			$listHtml = '<ul>';
			foreach($disciplines as $discipline) {
				$disciplineUI 	= new DisciplineUI($discipline);
				$listHtml		.= $disciplineUI->getAdminNavigationListItemHtml();
			}
			$listHtml .= '</ul>';

			return $listHtml;
		}

	}

?>