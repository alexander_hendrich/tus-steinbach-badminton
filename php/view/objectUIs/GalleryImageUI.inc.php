<?php
	class GalleryImageUI {
		### attributes
		private $galleryImage;
		
		### methodes
		
		public function __construct($galleryImage) {
			$this->galleryImage 	= $galleryImage;
		}
		
		public function getCoverImageHtml() {
			$pathToGallery = $this->galleryImage->getGallery()->getPath();
			$fileName = $this->galleryImage->getRank();
			$path = $pathToGallery.'/cover/'.$fileName.'.jpg';
			
			$html = '<img src="'.$path.'" class="coverImage" />';
			
			return $html;
		}
		
		public function getThumbListItemHtml() {
			$pathToGallery = $this->galleryImage->getGallery()->getPath();
			$fileName = $this->galleryImage->getRank();
			$pathToThumb	= $pathToGallery.'/thumbs/'.$fileName.'.jpg';
			$pathToBig		= $pathToGallery.'/big/'.$fileName.'.jpg';
			
			$html = '<li><a href="'.$pathToBig.'"><img src="'.$pathToThumb.'" /></a></li>';
			
			return $html;
		}
		
		
		public static function getThumbListHtml($galleryImages) {
			$thumbListItemsHtml = '';
			foreach($galleryImages as $galleryImage) {
				$galleryImageUI = new GalleryImageUI($galleryImage);
				$thumbListItemsHtml .= $galleryImageUI->getThumbListItemHtml();
			}
			
			$html = '<ul class="galleryThumbs galleryOmega36 ">'
						.$thumbListItemsHtml
					.'</ul>';
			
			return $html;
		}
		
	}

?>