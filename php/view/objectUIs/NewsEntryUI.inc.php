<?php
	class NewsEntryUI {
		### attributes
		private $newsEntry;
		
		### methodes
		
		public function __construct($newsEntry) {
			$this->newsEntry = $newsEntry;
		}
		
		public function getPreviewSectionHtml() {
			$newsEntry = $this->newsEntry;
		
			$key			= $newsEntry->getKey();
			$date			= date('d.m.Y', $newsEntry->getDateTime());
			$headline		= HtmlView::formatStringToHtml($newsEntry->getHeadline());
			$summary		= HtmlView::formatBBCodeTextToHtml($newsEntry->getSummary());
			$numComments	= $newsEntry->getNumComments();
			
			if($newsEntry->hasGallery()) {
				$gallery 		= $newsEntry->getGallery();
				
				$numImages		= $gallery->getNumImages();
				
				if($gallery->hasCoverImage()) {
					$galleryUI		= new GalleryUI($gallery);
					$coverImageHtml	= $galleryUI->getCoverImageHtml();
				} else {
					$coverImageHtml = '';
				}
			} else {
				$numImages		= 0;
				$coverImageHtml = '';
			}
		
			$html = '<article class="contentBox newsEntryPreview">'
						.'<a href="/news/'.$key.'/" class="wrapper">'
							.'<header>'	
								.'<h2>'.$headline.'</h2>'
								.'<div class="info">'
									.'<span class="dateInfo">'.$date.'</span>'
									.'<span class="galleryInfo">'.$numImages.' Bild(er)</span>'
									.'<span class="commentsInfo">'.$numComments.' Kommentar(e)</span>'
								.'</div>'													
							.'</header>'
							.$coverImageHtml
							.'<p class="summary"><strong>'.$summary.'</strong></p>'
						.'</a>'
					.'</article>';
			
			return $html;
		}
		
		public function getArticleSectionHtml() {
			$newsEntry = $this->newsEntry;
		
			$key			= $newsEntry->getKey();
			$date			= date('d.m.Y', $newsEntry->getDateTime());
			$headline		= HtmlView::formatStringToHtml($newsEntry->getHeadline());
			$summary		= HtmlView::formatBBCodeTextToHtml($newsEntry->getSummary());
			$article		= HtmlView::formatBBCodeTextToHtml($newsEntry->getArticle());
			$numComments	= $newsEntry->getNumComments();;
			
			if($newsEntry->hasGallery()) {
				$gallery 		= $newsEntry->getGallery();
				
				$numImages		= $gallery->getNumImages();
			} else {
				$numImages		= 0;
			}
		
			$html = '<article class="contentBox newsEntryArticle">'
						.'<header>'	
							.'<h2>'.$headline.'</h2>'								
							.'<div class="info">'
								.'<span class="dateInfo">'.$date.'</span>'
								.'<span class="galleryInfo">'.$numImages.' Bild(er)</span>'
								.'<span class="commentsInfo">'.$numComments.' Kommentar(e)</span>'
							.'</div>'	
							.'<p class="summary"><strong>'.$summary.'</strong></p>'														
						.'</header>'
						.'<p class="article">'.$article.'</p>'
					.'</article>';
			
			return $html;
		}
		
		public function getRelatedNewsEntriesListHtml() {
			$listItemsHtml = '';
			foreach($this->newsEntry->getRelatedNewsEntries() as $newsEntry) {
				$newsEntryUI = new NewsEntryUI($newsEntry);
				$listItemsHtml .= $newsEntryUI->getRelatedNewsEntriesListItemHtml();
			}
			
			$html = '<ul class="sidebarList">'
						.$listItemsHtml
					.'</ul>';
			
			return $html;
		}
		
		public function getRelatedNewsEntriesListItemHtml() {
			$newsEntry = $this->newsEntry;

			$key		= $newsEntry->getKey();
			$headline	= HtmlView::formatStringToHtml($newsEntry->getHeadline());
			
			
			$html = '<li><a href="/news/'.$key.'/">'.$headline.'</a></li>';
			
			return $html;
		}
	}
?>