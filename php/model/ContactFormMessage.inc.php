<?php
	class ContactFormMessage {
		### attributes
		private $receiver;
		private $name;
		private $email;
		private $subject;
		private $message;
		
		private $mailHandler;
		
		### methodes
		
		public function __construct($mailHandler) {
			$this->mailHandler = $mailHandler;
		}
		
		public function setReceiver($receiver) {
			//validate
			if($receiver !== 'abteilungsleitung' && $receiver !== 'jugendmannschaft' && $receiver !== 'altkoenigturnier' && $receiver !== 'webmaster') {
				throw new InvalidContactFormReceiverException();
			}
			
			$this->receiver = $receiver;
		}
		
		public function setName($name) {
			//validate
			if(empty($name)) {
				throw new EmptyException();
			}
			if(preg_match('/[\^<,\"@\/\{\}\(\)\*\$%\?=>:\|;#]+/i', $name)) {
				throw new InvalidCharactersException();
			}
			
			$this->name = $name;
		}
		
		public function setEMail($email) {
			//validate
			if(empty($email)) {
				throw new EmptyException();
			}
			if(filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
				throw new InvalidEMailException();
			}
			
			$this->email = $email;
		}
		
		public function setSubject($subject) {
			//validate
			if(empty($subject)) {
				throw new EmptyException();
			}
			
			$this->subject = $subject;
		}
		
		public function setMessage($message) {
			//validate
			if(empty($message)) {
				throw new EmptyException();
			}
			
			$this->message = $message;
		}
		
		public function send() {
			//prepare message
			$to		= $this->receiver.'@'.DOMAIN;
			$from	= array($this->email => $this->name);
			
			$body =	$this->message.MAIL_EOL
					.MAIL_EOL.MAIL_DIVIDER.MAIL_EOL.MAIL_EOL
					.'Diese E-Mail wurde über das Online-Kontaktformular versendet.';
			
			//prepare mail
			$mail = $this->mailHandler->getNewMailInstance();
			$mail->setSubject($this->subject);
			$mail->setFrom($from);
			$mail->setTo($to);		
			$mail->setBody($body);
			
			//send mail
			$this->mailHandler->send($mail);
		}

		
	}
?>