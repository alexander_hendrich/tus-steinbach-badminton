<?php
	class NewsEntryDB extends KeyObjectDB {
		### attributes
		private $commentDB;
		private $galleryDB;

		### methodes

		public function getByPageNum($pageNum, $newsEntriesPerPage) {
			$select = $this->buildSelectQuery();
			$select->setOrderBy(array('dateTime' => 'DESC'));
			$select->setLimit($newsEntriesPerPage);
			$select->setOffset(($pageNum-1)*$newsEntriesPerPage);
			$stmt = $select->run();
			return $this->getObjectsFromSelectStatement($stmt);
		}

		public function getRelatedByNewsEntryID($newsEntryId) {
			//build query
			$query = 	'SELECT `newsEntries`.*  '
						.'FROM `'.TABLE_NAME_NEWS_ENTRIES_TAGS_RELATION.'` AS `rel1`, '
								.'`'.TABLE_NAME_NEWS_ENTRIES_TAGS_RELATION.'` AS `rel2`, '
								.'`'.TABLE_NAME_NEWS_ENTRIES.'` AS `newsEntries` '
						.'WHERE `rel1`.`newsEntryID` = :newsEntryID '
							.'AND `rel2`.`newsEntryID` != :newsEntryID '
							.'AND `rel1`.`tagID` = `rel2`.`tagID` '
							.'AND `rel2`.`newsEntryID` = `newsEntries`.`id` '
						.'GROUP BY `rel2`.`newsEntryID` '
						.'ORDER BY COUNT(`rel2`.`newsEntryID`) DESC, RAND() '
						.'LIMIT '.NEWS_ENTRY_PAGE_NUM_RELATED_NEWS_ENTRIES;

			//get preparedStatement
			$stmt = $this->dbHandler->getPreparedStatement($query);

			//bind values
			$stmt->bindValue(':newsEntryID', $newsEntryId);

			//execute stmt/query
			$stmt->execute();

			//get objects from stmt
			$objects = $this->getObjectsFromSelectStatement($stmt);

			return $objects;
		}

		public function add($newsEntry) {
			throw new NotImplementedException();
		}

		public function update($newsEntry) {
			throw new NotImplementedException();
		}

		public function createObjectWithInitialValues($initialValues, $convertSqlToPhp=false) {
			//dependencies
		}

		public function createEmptyObject() {
			$galleryDB		= $this->galleryDB;
			$commentDB		= $this->commentDB;
			$newsEntryDB	= $this;

			return new NewsEntry($galleryDB, $commentDB, $newsEntryDB);
		}

		public function setCommentDB($commentDB) {
			$this->commentDB = $commentDB;
		}

		public function setGalleryDB($galleryDB) {
			$this->galleryDB = $galleryDB;
		}

	}
?>