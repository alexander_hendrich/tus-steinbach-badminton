<?php
	class DisciplineDB extends ObjectDB {
		### attributes
		private $entryDB;

		### methodes

		public function getAll() {
			$select = $this->buildSelectQuery();
			$select->setOrderBy(array('group' => 'ASC', 'type' => 'ASC'));
			$stmt = $select->run();
			return $this->getObjectsFromSelectStatement($stmt);
		}

		public function add($discipline) {
			throw new NotImplementedException();
		}

		public function update($discipline) {
			throw new NotImplementedException();
		}

		public function createEmptyObject() {
			$entryDB = $this->entryDB;

			return new Discipline($entryDB);
		}

		public function setEntryDB($entryDB) {
			$this->entryDB = $entryDB;
		}
	}
?>