<?php
	class GalleryImageDB extends ObjectDB {
		### attributes
		private $galleryDB;

		### methodes

		public function getByGalleryID($galleryID) {
			$select = $this->buildSelectQuery();
			$select->setWhere(array('galleryID' => $galleryID));
			$select->setOrderBy(array('rank' => 'ASC'));
			$stmt = $select->run();
			return $this->getObjectsFromSelectStatement($stmt);
		}

		public function add($galleryImage) {
			throw new NotImplementedException();
		}

		public function update($galleryImage) {
			throw new NotImplementedException();
		}

		public function createEmptyObject() {
			$galleryDB = $this->galleryDB;

			return new GalleryImage($galleryDB);
		}

		public function setGalleryDB($galleryDB) {
			$this->galleryDB = $galleryDB;
		}

	}
?>