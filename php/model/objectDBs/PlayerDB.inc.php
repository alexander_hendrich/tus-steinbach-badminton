<?php
	class PlayerDB extends ObjectDB {
		### attributes
		private $squadDB;

		### methodes

		public function getBySquadIDAndGender($squadID, $gender) {
			$select = $this->buildSelectQuery();
			$select->setWhere(array('squadID' => $squadID, 'gender' => $gender));
			$select->setOrderBy(array('rank' => 'ASC'));
			$stmt = $select->run();
			return $this->getObjectsFromSelectStatement($stmt);
		}

		public function add($player) {
			throw new NotImplementedException();
		}

		public function update($player) {
			throw new NotImplementedException();
		}

		public function createEmptyObject() {
			$squadDB = $this->squadDB;

			return new Player($squadDB);
		}

		public function setSquadDB($squadDB) {
			$this->squadDB = $squadDB;
		}
	}
?>