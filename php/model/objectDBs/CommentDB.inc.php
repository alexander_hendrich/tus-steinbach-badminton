<?php
	class CommentDB extends ObjectDB {
		### attributes
		private $newsEntryDB;

		### methodes

		public function getByNewsEntryID($newsEntryID) {
			$select = $this->buildSelectQuery();
			$select->setWhere(array('newsEntryID' => $newsEntryID));
			$select->setOrderBy(array('dateTime' => 'ASC'));
			$stmt = $select->run();
			return $this->getObjectsFromSelectStatement($stmt);
		}

		public function add($comment) {
			$values = array(
				'newsEntryID'	=> $comment->getNewsEntryID(),
				'dateTime'		=> date('Y.m.d H:i:s', $comment->getDateTime()),
				'authorName'	=> $comment->getAuthorName(),
				'authorEMail'	=> $comment->getAuthorEMail(),
				'text'			=> $comment->getText()
			);

			$insertQuery = $this->buildInsertQuery();
			$insertQuery->setValues($values);
			$id = $insertQuery->run();

			$comment->setID($id);
			$this->cache($comment);
		}

		public function update($comment) {
			throw new NotImplementedException();
		}

		public function createEmptyObject() {
			$newsEntryDB = $this->newsEntryDB;

			return new Comment($newsEntryDB);
		}

		public function setNewsEntryDB($newsEntryDB) {
			$this->newsEntryDB = $newsEntryDB;
		}
	}
?>