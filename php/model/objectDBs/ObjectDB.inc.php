<?php
	abstract class ObjectDB implements IObserver {
		### attributes
		protected $dbHandler;
		protected $tableName;

		protected $cache = array();

		protected $countAll;

		### methodes

		public function __construct($dbHandler, $tableName) {
			$this->dbHandler	= $dbHandler;
			$this->tableName	= $tableName;
		}

		public function getByID($id) {
			if($this->isCacheHit($id)) {
				//load from cache
				$object = $this->getFromCache($id);
			} else {
				//load from db
				$select = $this->buildSelectQuery();
				$select->setWhere(array('id' => $id));
				$stmt = $select->run();
				$objects = $this->getObjectsFromSelectStatement($stmt);

				return $this->getSingleObject($objects);
			}

			return $object;
		}

		public function getCountAll() {
			if($this->countAll === null) {
				$this->loadCountAll();
			}
			return $this->countAll;
		}

		public function isValidID($id) {
			return ($this->getByID($id) !== null);
		}

		abstract public function add($object);

		abstract public function update($object);

		abstract public function createEmptyObject();


		protected function cache($object) {
			$this->cache[$object->getID()] = $object;
		}

		protected function buildSelectQuery() {
			return new SelectQuery($this->dbHandler, $this->tableName);
		}

		protected function buildInsertQuery() {
			return new InsertQuery($this->dbHandler, $this->tableName);
		}

		protected function buildUpdateQuery() {
			return new UpdateQuery($this->dbHandler, $this->tableName);
		}

		protected function getObjectsFromSelectStatement($stmt) {
			//set fetchMode
			$stmt->setFetchMode(PDO::FETCH_ASSOC);

			//get object(s) from preparedStatement
			$objects = array();
			while($rowData = $stmt->fetch()) {
				if($this->isCacheHit($rowData['id'])) {
					//cache-hit
					$object = $this->getFromCache($rowData['id']);

				} else {
					$object = $this->createEmptyObject();
					$object->loadDataFromSqlRow($rowData);
					$object->addObserver($this);
					$this->cache($object);
				}
				$objects[] = $object;
			}

			return $objects;
		}

		private function loadCountAll() {
			$query = 'SELECT COUNT(*) AS `count` FROM `'.$this->tableName.'`';
			$stmt = $this->dbHandler->getPreparedStatement($query);
			$stmt->execute();
			$stmt->setFetchMode(PDO::FETCH_ASSOC);
			$result = $stmt->fetch();
			$this->countAll = $result['count'];
		}

		private function isCacheHit($id) {
			return (array_key_exists($id, $this->cache) && empty($this->cache[$id]) === false);
		}

		private function getFromCache($id) {
			return $this->cache[$id];
		}

		protected function getSingleObject($objects) {
			if(count($objects) === 1) {
				return $objects[0];
			} else {
				return null;
			}
		}
	}
?>