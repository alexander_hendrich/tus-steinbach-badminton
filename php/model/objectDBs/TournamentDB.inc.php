<?php
	class TournamentDB extends KeyObjectDB {
		### attributes

		### methodes


		public function getFutureTournaments() {
			//build query
			$query = 	'SELECT * '
						.'FROM `'.$this->tableName.'` '
						.'WHERE `dateEnd` >= CURDATE() '
						.'ORDER BY `dateStart`';

			//get preparedStatement
			$stmt = $this->dbHandler->getPreparedStatement($query);

			//execute stmt/query
			$stmt->execute();

			//get objects from stmt
			$objects = $this->getObjectsFromSelectStatement($stmt);

			return $objects;

		}

		public function add($tournament) {
			throw new NotImplementedException();
		}

		public function update($tournament) {
			throw new NotImplementedException();
		}

		public function createEmptyObject() {
			return new Tournament();
		}

	}
?>