<?php
	abstract class KeyObjectDB extends ObjectDB {
		### attributes
		
		protected $cacheIDsByKeys	= array();
		
		### methodes
		
		public function getByKey($key) {
			if($this->isCacheHitByKey($key)) {
				//load from cache
				$object = $this->getFromCacheByKey($key);
			} else {
				//load from db
				$select = $this->buildSelectQuery();
				$select->setWhere(array('key' => $key));
				$stmt = $select->run();
				$objects = $this->getObjectsFromSelectStatement($stmt);
				
				return $this->getSingleObject($objects);
			}
			
			return $object;
		}
		
		private function isCacheHitByKey($key) {
			return (array_key_exists($key, $this->cacheIDsByKeys) && array_key_exists($this->cacheIDsByKeys[$key], $this->cache));
		}
		
		private function getFromCacheByKey($key) {
			$id = $this->cacheIDsByKeys[$key];
			return $this->cache[$id];
		}
		
		protected function cache($object) {
			parent::cache($object);					
			//store id and key
			$this->cacheIDsByKeys[$object->getKey()] = $object->getID();
		}
		
		public function isValidKey($key) {
			return ($this->getByKey($key) !== null);
		}
		
	}
?>