<?php
	class UpdateQuery extends SqlQuery {
		### attributes
		private $values;

		### methodes

		public function setValues($values) {
			$this->values = $values;
		}

		public function setWhere($where) {
			$this->where = $where;
		}

		public function run() {
			//build queryString
			$queryString = $this->buildQueryString();

			//get preparedStatement
			$stmt = $this->dbHandler->getPreparedStatement($queryString);

			//bind values
			foreach($this->values as $columnName=>$value) {
				$stmt->bindValue(':'.$columnName.'Values', $value);
			}
			foreach($this->where as $columnName=>$value) {
				$stmt->bindValue(':'.$columnName.'Where', $value);
			}

			//execute query
			$stmt->execute();
		}

		protected function buildQueryString() {
			//build query
			$queryString = 'UPDATE `'.$this->tableName.'`';

			//valuesClause
			$valuesClause = ' SET ';
			$first = true;
			foreach($this->values as $columnName=>$value) {
				if($first) {
					$valuesClause .= '`'.$columnName.'` = :'.$columnName.'Values';
					$first = false;
				} else {
					$valuesClause .= ', `'.$columnName.'` = :'.$columnName.'Values';
				}
			}
			$queryString .= $valuesClause;

			//whereClause
			$whereClause = ' WHERE ';
			$first = true;
			foreach($this->where as $columnName=>$value) {
				if($first) {
					$whereClause .= '`'.$columnName.'` = :'.$columnName.'Where';
					$first = false;
				} else {
					$whereClause .= ' AND `'.$columnName.'` = :'.$columnName.'Where';
				}
			}
			$queryString .= $whereClause;

			return $queryString;
		}
	}
?>