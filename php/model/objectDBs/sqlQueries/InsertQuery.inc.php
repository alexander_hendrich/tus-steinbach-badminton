<?php
	class InsertQuery extends SqlQuery {
		### attributes
		private $values;

		### methodes

		public function setValues($values) {
			$this->values = $values;
		}

		public function run() {
			//build queryString
			$queryString = $this->buildQueryString();

			//get preparedStatement
			$stmt = $this->dbHandler->getPreparedStatement($queryString);

			//bind values
			foreach($this->values as $columnName=>$value) {
				$stmt->bindValue(':'.$columnName, $value);
			}

			//execute query
			$stmt->execute();

			return $this->dbHandler->lastInsertID();

		}

		protected function buildQueryString() {
			//build query
			$queryString = 'INSERT INTO `'.$this->tableName.'`';

			//valuesClause
			$valuesClause = ' SET ';
			$first = true;
			foreach($this->values as $columnName=>$value) {
				if($first) {
					$valuesClause .= '`'.$columnName.'` = :'.$columnName.'';
					$first = false;
				} else {
					$valuesClause .= ', `'.$columnName.'` = :'.$columnName.'';
				}
			}
			$queryString .= $valuesClause;

			return $queryString;
		}
	}
?>