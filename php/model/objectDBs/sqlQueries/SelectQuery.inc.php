<?php
	class SelectQuery extends SqlQuery {
		### attributes
		private $where;
		private $groupBy;
		private $orderBy;
		private $limit;
		private $offset;

		### methodes

		public function setWhere($where) {
			$this->where = $where;
		}

		public function setGroupBy($groupBy) {
			$this->groupBy = $groupBy;
		}

		public function setOrderBy($orderBy) {
			$this->orderBy = $orderBy;
		}

		public function setLimit($limit) {
			$this->limit = $limit;
		}

		public function setOffset($offset) {
			$this->offset = $offset;
		}

		public function run() {
			//build queryString
			$queryString = $this->buildQueryString();

			//get preparedStatement
			$stmt = $this->dbHandler->getPreparedStatement($queryString);

			//bind values
			if($this->where !== null) {
				foreach($this->where as $columnName=>$value) {
					$stmt->bindValue(':'.$columnName, $value);
				}
			}

			//execute query
			$stmt->execute();

			return $stmt;

		}

		protected function buildQueryString() {
			//build query
			$queryString = 'SELECT * FROM `'.$this->tableName.'`';

			//whereClause
			if($this->where !== null) {
				$whereClause = ' WHERE ';
				$first = true;
				foreach($this->where as $columnName=>$value) {
					if($first) {
						$whereClause .= '`'.$columnName.'` = :'.$columnName.'';
						$first = false;
					} else {
						$whereClause .= ' AND `'.$columnName.'` = :'.$columnName.'';
					}
				}
				$queryString .= $whereClause;
			}

			//groupByClause
				//TODO

			//orderByClause
			if($this->orderBy !== null) {
				$orderByClause = ' ORDER BY ';
				$first = true;
				foreach($this->orderBy as $columnName=>$direction) {
					if($first) {
						$orderByClause .= '`'.$columnName.'` '.$direction.'';
						$first = false;
					} else {
						$orderByClause .= ', `'.$columnName.'` '.$direction.'';
					}
				}
				$queryString .= $orderByClause;
			}

			//limitClause
			if($this->limit !== null) {
				$limitClause = ' LIMIT '.$this->limit;
				$queryString .= $limitClause;
			}

			//offsetClause
			if($this->offset !== null) {
				$offsetClause = ' OFFSET '.$this->offset;
				$queryString .= $offsetClause;
			}

			return $queryString;
		}
	}
?>