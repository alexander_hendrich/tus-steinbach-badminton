<?php
	abstract class SqlQuery {
		### attributes
		protected $dbHandler;
		protected $tableName;
		
		### methodes
		
		public function __construct($dbHandler, $tableName) {
			$this->dbHandler	= $dbHandler;
			$this->tableName	= $tableName;
		}
		
		abstract protected function buildQueryString();
		
		abstract public function run();
	}
?>