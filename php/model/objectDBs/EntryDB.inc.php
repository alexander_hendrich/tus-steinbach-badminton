<?php
	class EntryDB extends ObjectDB {
		### attributes
		private $disciplineDB;

		### methodes

		public function getAcceptedByDisciplineID($disciplineID) {
			$select = $this->buildSelectQuery();
			$select->setWhere(array('disciplineID' => $disciplineID, 'state' => 'accepted'));
			$select->setOrderBy(array('dateTimeAccepted' => 'ASC'));
			$stmt = $select->run();
			return $this->getObjectsFromSelectStatement($stmt);
		}

		public function getPendingByDisciplineID($disciplineID) {
			$select = $this->buildSelectQuery();
			$select->setWhere(array('disciplineID' => $disciplineID, 'state' => 'pending'));
			$select->setOrderBy(array('dateTimeRegistered' => 'ASC'));
			$stmt = $select->run();
			return $this->getObjectsFromSelectStatement($stmt);
		}

		public function getWaitingByDisciplineID($disciplineID) {
			$select = $this->buildSelectQuery();
			$select->setWhere(array('disciplineID' => $disciplineID, 'state' => 'waiting'));
			$select->setOrderBy(array('dateTimeWaiting' => 'ASC'));
			$stmt = $select->run();
			return $this->getObjectsFromSelectStatement($stmt);
		}

		public function add($entry) {
			$values = array(
				'disciplineID'			=> $entry->getDisciplineID(),
				'email'					=> $entry->getEMail(),
				'state'					=> $entry->getState(),
				'dateTimeRegistered'	=> ($entry->getDateTimeRegistered() !== null) ? date('Y.m.d H:i:s', $entry->getDateTimeRegistered()) : null,
				'dateTimeAccepted'		=> ($entry->getDateTimeAccepted() !== null) ? date('Y.m.d H:i:s', $entry->getDateTimeAccepted()) : null,
				'dateTimeWaiting'		=> ($entry->getDateTimeWaiting() !== null) ? date('Y.m.d H:i:s', $entry->getDateTimeWaiting()) : null,
				'player1Name'			=> $entry->getPlayer1Name(),
				'player2Name'			=> $entry->getPlayer2Name(),
				'player1Squad'			=> $entry->getPlayer1Squad(),
				'player2Squad'			=> $entry->getPlayer2Squad()
			);

			$insertQuery = $this->buildInsertQuery();
			$insertQuery->setValues($values);
			$id = $insertQuery->run();

			$entry->setID($id);
			$this->cache($entry);
		}

		public function update($entry) {
			$id = $entry->getID();
			$values = array(
				'disciplineID'			=> $entry->getDisciplineID(),
				'email'					=> $entry->getEMail(),
				'state'					=> $entry->getState(),
				'dateTimeRegistered'	=> ($entry->getDateTimeRegistered() !== null) ? date('Y.m.d H:i:s', $entry->getDateTimeRegistered()) : null,
				'dateTimeAccepted'		=> ($entry->getDateTimeAccepted() !== null) ? date('Y.m.d H:i:s', $entry->getDateTimeAccepted()) : null,
				'dateTimeWaiting'		=> ($entry->getDateTimeWaiting() !== null) ? date('Y.m.d H:i:s', $entry->getDateTimeWaiting()) : null,
				'player1Name'			=> $entry->getPlayer1Name(),
				'player2Name'			=> $entry->getPlayer2Name(),
				'player1Squad'			=> $entry->getPlayer1Squad(),
				'player2Squad'			=> $entry->getPlayer2Squad()
			);

			$updateQuery = $this->buildUpdateQuery();
			$updateQuery->setValues($values);
			$updateQuery->setWhere(array('id' => $id));
			$updateQuery->run();

			//update dependable objects
			$entry->getDiscipline()->loadEntries();
		}

		public function createEmptyObject() {
			$disciplineDB = $this->disciplineDB;

			return new Entry($disciplineDB);
		}

		public function setDisciplineDB($disciplineDB) {
			$this->disciplineDB = $disciplineDB;
		}
	}
?>