<?php
	class UserDB extends ObjectDB {
		### attributes

		### methodes

		public function getByEMail($email) {
			$select = $this->buildSelectQuery();
			$select->setWhere(array('email' => $email));
			$stmt = $select->run();
			$objects = $this->getObjectsFromSelectStatement($stmt);

			return $this->getSingleObject($objects);
		}

		public function add($user) {
			throw new NotImplementedException();
		}

		public function update($user) {
			throw new NotImplementedException();
		}

		public function createEmptyObject() {
			return new User();
		}
	}
?>