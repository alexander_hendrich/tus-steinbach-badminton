<?php
	class Player extends DBOBject {
		### attributes
		private $id;
		private $name;
		private $gender;
		private $squadID;
		private $rank;
		private $alleturnierePlayerID;

		private $squad;

		private $squadDB;


		### methodes

		public function __construct($squadDB) {
			$this->squadDB	= $squadDB;
		}

		public function loadDataFromSqlRow($rowData) {
			$this->id					= $rowData['id'];
			$this->name					= $rowData['name'];
			$this->gender				= $rowData['gender'];
			$this->squadID				= $rowData['squadID'];
			$this->rank					= $rowData['rank'];
			$this->alleturnierePlayerID	= $rowData['alleturnierePlayerID'];
		}

		public function getID() {
			return $this->id;
		}

		public function getName() {
			return $this->name;
		}

		public function getRank() {
			return $this->rank;
		}

		public function getAlleturnierePlayerID() {
			return $this->alleturnierePlayerID;
		}

		public function getSquad() {
			if($this->squad === null) {
				$this->loadSquad();
			}
			return $this->squad;
		}

		private function loadSquad() {

			$this->squad = $this->squadDB->getByID($this->squadID);
		}
	}
?>