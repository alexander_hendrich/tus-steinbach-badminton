<?php
	class Discipline extends DBObject {
		### attributes
		private $id;
		private $group;
		private $type;
		private $numMaxEntries;

		private $entriesAccepted;
		private $entriesPending;
		private $entriesWaiting;

		private $entryDB;


		### methodes

		public function __construct($entryDB) {
			$this->entryDB	= $entryDB;
		}

		public function loadDataFromSqlRow($rowData) {
			$this->id				= $rowData['id'];
			$this->group			= $rowData['group'];
			$this->type				= $rowData['type'];
			$this->numMaxEntries	= $rowData['numMaxEntries'];
		}

		public function getID() {
			return $this->id;
		}

		public function getGroup() {
			return $this->group;
		}

		public function getType() {
			return $this->type;
		}

		public function getNumMaxEntries() {
			return $this->numMaxEntries;
		}

		public function getEntriesAccepted() {
			if($this->entriesAccepted === null) {
				$this->loadEntries();
			}
			return $this->entriesAccepted;
		}

		public function getEntriesWaiting() {
			if($this->entriesWaiting === null) {
				$this->loadEntries();
			}
			return $this->entriesWaiting;
		}

		public function getEntriesPending() {
			if($this->entriesPending === null) {
				$this->loadEntries();
			}
			return $this->entriesPending;
		}

		public function getNumEntriesAccepted() {
			if($this->entriesAccepted === null) {
				$this->loadEntries();
			}
			return count($this->entriesAccepted);
		}

		public function getNumEntriesWaiting() {
			if($this->entriesWaiting === null) {
				$this->loadEntries();
			}
			return count($this->entriesWaiting);
		}

		public function getNumEntriesPending() {
			if($this->entriesPending === null) {
				$this->loadEntries();
			}
			return count($this->entriesPending);
		}

		public function isDouble() {
			return ($this->type === 'DD' || $this->type === 'HD' || $this->type === 'MIX');
		}

		public function isFull() {
			$numEntriesAccepted = $this->getNumEntriesAccepted();
			return ($numEntriesAccepted >= $this->numMaxEntries);
		}

		public function loadEntries() {
			$this->entriesAccepted	= $this->entryDB->getAcceptedByDisciplineID($this->id);
			$this->entriesPending	= $this->entryDB->getPendingByDisciplineID($this->id);
			$this->entriesWaiting	= $this->entryDB->getWaitingByDisciplineID($this->id);
		}
}
?>