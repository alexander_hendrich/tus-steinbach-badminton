<?php
	class GalleryImage extends DBOBject {
		### attributes
		private $id;
		private $galleryID;
		private $rank;

		private $gallery;

		private $galleryDB;

		### methodes

		public function __construct($galleryDB) {
			$this->galleryDB	= $galleryDB;
		}

		public function loadDataFromSqlRow($rowData) {
			$this->id			= $rowData['id'];
			$this->galleryID	= $rowData['galleryID'];
			$this->rank			= $rowData['rank'];
		}

		public function getID() {
			return $this->id;
		}

		public function getRank() {
			return $this->rank;
		}

		public function getGallery() {
			if($this->gallery === null) {
				$this->loadGallery();
			}
			return $this->gallery;
		}

		private function loadGallery() {
			$this->gallery = $this->galleryDB->getByID($this->galleryID);
		}
	}
?>