<?php
	class Comment extends DBObject {
		### attributes
		private $id;
		private $newsEntryID;
		private $dateTime;
		private $authorName;
		private $authorEMail;
		private $text;

		private $newsEntry;

		private $newsEntryDB;


		### methodes

		public function __construct($newsEntryDB) {
			$this->newsEntryDB	= $newsEntryDB;
		}

		public function loadDataFromSqlRow($rowData) {
			$this->id			= $rowData['id'];
			$this->newsEntryID	= $rowData['newsEntryID'];
			$this->dateTime		= strtotime($rowData['dateTime']);
			$this->authorName	= $rowData['authorName'];
			$this->authorEMail	= $rowData['authorEMail'];
			$this->text			= $rowData['text'];
		}

		public function getID() {
			return $this->id;
		}

		public function getNewsEntryID() {
			return $this->newsEntryID;
		}

		public function getDateTime() {
			return $this->dateTime;
		}

		public function getAuthorName() {
			return $this->authorName;
		}

		public function getAuthorEMail() {
			return $this->authorEMail;
		}

		public function getText() {
			return $this->text;
		}

		public function getNewsEntry() {
			if($this->newsEntry === null) {
				$this->loadNewsEntry();
			}
			return $this->newsEntry;
		}

		public function setID($id) {
			//validate
			if($this->id !== null || is_numeric($id) === false) {
				throw new IllegalIDOverrideException();
			}

			$this->id = $id;
		}

		public function setNewsEntryID($newsEntryID) {
			if($this->newsEntryID !== $newsEntryID) {
				//validate
				if($this->newsEntryDB->isValidID($newsEntryID) === false) {
					throw new InvalidIDException();
				}

				$this->newsEntryID = $newsEntryID;
				$this->setChanged();
			}
		}

		public function setDateTime($dateTime) {
			if($this->dateTime !== $dateTime) {
				//validate
				if(empty($dateTime)) {
					throw new EmptyException();
				}
				if($dateTime !== strtotime(date('d.m.Y H:i:s', $dateTime))) {
					throw new InvalidDateTimeException();
				}

				$this->dateTime = $dateTime;
				$this->setChanged();
			}
		}

		public function setAuthorName($authorName) {
			if($this->authorName !== $authorName) {
				//validate
				if(empty($authorName)) {
					throw new EmptyException();
				}
				if(preg_match('/[\^<,\"@\/\{\}\(\)\*\$%\?=>:\|;#]+/i', $authorName)) {
					throw new InvalidCharactersException();
				}

				$this->authorName = $authorName;
				$this->setChanged();
			}
		}

		public function setAuthorEMail($authorEMail) {
			if($this->authorEMail !== $authorEMail) {
				//validate
				if(empty($authorEMail)) {
					throw new EmptyException();
				}
				if(filter_var($authorEMail, FILTER_VALIDATE_EMAIL) === false) {
					throw new InvalidEMailException();
				}

				$this->authorEMail = $authorEMail;
				$this->setChanged();
			}
		}

		public function setText($text) {
			if($this->text !== $text) {
				//validate
				if(empty($text)) {
					throw new EmptyException();
				}

				$this->text = $text;
				$this->setChanged();
			}
		}

		private function loadNewsEntry() {
			$this->newsEntry = $this->newsEntryDB->getByID($this->newsEntryID);
		}
	}
?>