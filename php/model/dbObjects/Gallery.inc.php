<?php
	class Gallery extends DBObject {
		### attributes
		private $id;
		private $key;
		private $coverImageID;

		private $images;
		private $coverImage;

		private $galleryImageDB;

		### methodes

		public function __construct($galleryImageDB) {
			$this->galleryImageDB = $galleryImageDB;
		}

		public function loadDataFromSqlRow($rowData) {
			$this->id			= $rowData['id'];
			$this->key			= $rowData['key'];
			$this->coverImageID	= $rowData['coverImageID'];
		}

		public function getID() {
			return $this->id;
		}

		public function getKey() {
			return $this->key;
		}

		public function getPath() {
			return '/images/galleries/'.$this->key;
		}

		public function getCoverImage() {
			if($this->coverImage === null && $this->coverImageID !== null) {
				$this->loadCoverImage();
			}
			return $this->coverImage;

		}

		public function getImages() {
			if($this->images === null) {
				$this->loadImages();
			}
			return $this->images;
		}

		public function getNumImages() {
			if($this->images === null) {
				$this->loadImages();
			}
			return count($this->images);
		}

		public function getPathToGallery() {
			return '/images/galleries/'.$this->key;
		}

		public function hasCoverImage() {
			if($this->coverImage === null && $this->coverImageID !== null) {
				$this->loadCoverImage();
			}
			return ($this->coverImage !== null);
		}

		private function loadImages() {
			$this->images = $this->galleryImageDB->getByGalleryID($this->id);
		}

		private function loadCoverImage() {
			$this->coverImage = $this->galleryImageDB->getByID($this->coverImageID);
		}
	}
?>