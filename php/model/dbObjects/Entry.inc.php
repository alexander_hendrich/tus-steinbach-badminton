<?php
	class Entry extends DBObject {
		### attributes
		private $id;
		private $disciplineID;
		private $email;
		private $state;
		private $dateTimeRegistered;
		private $dateTimeAccepted;
		private $dateTimeWaiting;
		private $player1Name;
		private $player2Name;
		private $player1Squad;
		private $player2Squad;

		private $discipline;

		private $disciplineDB;

		### methodes

		public function __construct($disciplineDB) {
			$this->disciplineDB = $disciplineDB;
		}

		public function loadDataFromSqlRow($rowData) {
			$this->id					= $rowData['id'];
			$this->disciplineID			= $rowData['disciplineID'];
			$this->email				= $rowData['email'];
			$this->state				= $rowData['state'];
			$this->dateTimeRegistered	= ($rowData['dateTimeRegistered'] !== null) ? strtotime($rowData['dateTimeRegistered']) : null;
			$this->dateTimeAccepted		= ($rowData['dateTimeAccepted'] !== null) ? strtotime($rowData['dateTimeAccepted']) : null;
			$this->dateTimeWaiting		= ($rowData['dateTimeWaiting'] !== null) ? strtotime($rowData['dateTimeWaiting']) : null;
			$this->player1Name			= $rowData['player1Name'];
			$this->player2Name			= $rowData['player2Name'];
			$this->player1Squad			= $rowData['player1Squad'];
			$this->player2Squad			= $rowData['player2Squad'];
		}

		public function getID() {
			return $this->id;
		}

		public function getDisciplineID() {
			return $this->disciplineID;
		}

		public function getEMail() {
			return $this->email;
		}

		public function getState() {
			return $this->state;
		}

		public function getDateTimeRegistered() {
			return $this->dateTimeRegistered;
		}

		public function getDateTimeAccepted() {
			return $this->dateTimeAccepted;
		}

		public function getDateTimeWaiting() {
			return $this->dateTimeWaiting;
		}

		public function getPlayer1Name() {
			return $this->player1Name;
		}

		public function getPlayer2Name() {
			return $this->player2Name;
		}

		public function getPlayer1Squad() {
			return $this->player1Squad;
		}

		public function getPlayer2Squad() {
			return $this->player2Squad;
		}

		public function getDiscipline() {
			if($this->discipline === null) {
				$this->loadDiscipline();
			}
			return $this->discipline;
		}

		public function setID($id) {
			//validate
			if($this->id !== null || is_numeric($id) === false) {
				throw new IllegalIDOverrideException();
			}

			$this->id = $id;
		}

		public function setDisciplineID($disciplineID) {
			if($this->disciplineID !== $disciplineID) {
				//validate
				if($this->disciplineDB->isValidID($disciplineID) === false) {
					throw new InvalidIDException();
				}

				$this->disciplineID = $disciplineID;
				$this->setChanged();
			}
		}

		public function setState($state) {
			if($this->state !== $state) {
				//validate
				if($state !== 'pending' && $state !== 'accepted' && $state !== 'waiting') {
					throw new InvalidEntryStateException();
				}

				switch ($state) {
					case 'pending':
						$this->state				= 'pending';
						$this->dateTimeRegistered	= time();
						$this->dateTimeAccepted		= null;
						$this->dateTimeWaiting		= null;
						break;
					case 'accepted':
						//check discipline not full
						if($this->getDiscipline()->isFull()) {
							throw new DisciplineFullException();
						}
						$this->state			= 'accepted';
						$this->dateTimeAccepted	= time();
						break;
					case 'waiting':
						$this->state			= 'waiting';
						$this->dateTimeAccepted	= null;
						$this->dateTimeWaiting	= time();
						break;
				}
				$this->setChanged();
			}
		}

		public function setEMail($email) {
			if($this->email !== $email) {
				//validate
				if(empty($email)) {
					throw new EmptyException();
				}
				if(filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
					throw new InvalidEMailException();
				}

				$this->email = $email;
				$this->setChanged();
			}
		}

		public function setPlayer1Name($player1Name) {
			if($this->player1Name !== $player1Name) {
				//validate
				if(empty($player1Name)) {
					throw new EmptyException();
				}

				$this->player1Name = $player1Name;
				$this->setChanged();
			}
		}

		public function setPlayer2Name($player2Name) {
			if($this->player2Name !== $player2Name) {
				//validate
				if(empty($player2Name)) {
					throw new EmptyException();
				}

				$this->player2Name = $player2Name;
				$this->setChanged();
			}
		}

		public function setPlayer1Squad($player1Squad) {
			if($this->player1Squad !== $player1Squad) {
				//validate
				if(empty($player1Squad)) {
					throw new EmptyException();
				}

				$this->player1Squad = $player1Squad;
				$this->setChanged();
			}
		}

		public function setPlayer2Squad($player2Squad) {
			if($this->player2Squad !== $player2Squad) {
				//validate
				if(empty($player2Squad)) {
					throw new EmptyException();
				}

				$this->player2Squad = $player2Squad;
				$this->setChanged();
			}
		}



		private function loadDiscipline() {
			$this->discipline = $this->disciplineDB->getByID($this->disciplineID);
		}
	}
?>