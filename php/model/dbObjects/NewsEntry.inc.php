<?php
	class NewsEntry extends DBObject {
		### attributes
		private $id;
		private $key;
		private $galleryID;
		private $dateTime;
		private $headline;
		private $summary;
		private $article;

		private $comments;
		private $gallery;
		private $relatedNewsEntries;

		private $galleryDB;
		private $commentDB;
		private $newsEntryDB;

		### methodes

		public function __construct($galleryDB, $commentDB, $newsEntryDB) {
			$this->galleryDB	= $galleryDB;
			$this->commentDB	= $commentDB;
			$this->newsEntryDB	= $newsEntryDB;
		}

		public function loadDataFromSqlRow($rowData) {
			$this->id			= $rowData['id'];
			$this->key			= $rowData['key'];
			$this->galleryID	= $rowData['galleryID'];
			$this->dateTime		= strtotime($rowData['dateTime']);
			$this->headline		= $rowData['headline'];
			$this->summary		= $rowData['summary'];
			$this->article		= $rowData['article'];
		}

		public function getID() {
			return $this->id;
		}

		public function getKey() {
			return $this->key;
		}

		public function getDateTime() {
			return $this->dateTime;
		}

		public function getHeadline() {
			return $this->headline;
		}

		public function getSummary() {
			return $this->summary;
		}

		public function getArticle() {
			return $this->article;
		}

		public function getGallery() {
			if($this->gallery === null && $this->galleryID != null) {
				$this->loadGallery();
			}
			return $this->gallery;
		}

		public function getComments() {
			if($this->comments === null) {
				$this->loadComments();
			}
			return $this->comments;
		}

		public function getRelatedNewsEntries() {
			if($this->relatedNewsEntries === null) {
				$this->loadRelatedNewsEntries();
			}
			return $this->relatedNewsEntries;
		}

		public function getNumComments() {
			if($this->comments === null) {
				$this->loadComments();
			}
			return count($this->comments);
		}

		public function hasGallery() {
			if($this->gallery === null && $this->galleryID !== null) {
				$this->loadGallery();
			}
			return ($this->gallery !== null);
		}

		public function hasComments() {
			if($this->comments === null) {
				$this->loadComments();
			}
			return (count($this->comments) > 0);
		}

		public function hasRelatedNewsEntries() {
			if($this->relatedNewsEntries === null) {
				$this->loadRelatedNewsEntries();
			}
			return (count($this->relatedNewsEntries) > 0);
		}

		private function loadComments() {
			$this->comments = $this->commentDB->getByNewsEntryID($this->id);
		}

		private function loadGallery() {
			$this->gallery = $this->galleryDB->getByID($this->galleryID);
		}

		private function loadRelatedNewsEntries() {
			$this->relatedNewsEntries = $this->newsEntryDB->getRelatedByNewsEntryID($this->id);
		}
	}
?>