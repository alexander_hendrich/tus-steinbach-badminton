<?php
	class User extends DBObject {
		### attributes
		private $id;
		private $name;
		private $email;
		private $passwordHash;

		### methodes

		public function loadDataFromSqlRow($rowData) {
			$this->id			= $rowData['id'];
			$this->name			= $rowData['name'];
			$this->email		= $rowData['email'];
			$this->passwordHash	= $rowData['passwordHash'];
		}

		public function getID() {
			return $this->id;
		}

		public function getName() {
			return $this->name;
		}

		public function getEMail() {
			return $this->email;
		}

		public function checkPassword($password) {
			return ($this->passwordHash === sha1($password));
		}

	}
?>