<?php
	class Tournament extends DBObject {
		### attributes
		private $id;
		private $key;
		private $name;
		private $dateStart;
		private $dateEnd;
		private $dateDeadline;
		private $location;
		private $disciplines;
		private $leagues;
		private $websiteLink;


		### methodes

		public function loadDataFromSqlRow($rowData) {
			$this->id 			= $rowData['id'];
			$this->key 			= $rowData['key'];
			$this->name 		= $rowData['name'];
			$this->dateStart 	= strtotime($rowData['dateStart']);
			$this->dateEnd 		= strtotime($rowData['dateEnd']);
			$this->dateDeadline	= strtotime($rowData['dateDeadline']);
			$this->location 	= $rowData['location'];
			$this->disciplines 	= explode(',', $rowData['disciplines']);
			$this->leagues 		= explode(',', $rowData['leagues']);
			$this->websiteLink 	= $rowData['websiteLink'];
		}

		public function getID() {
			return $this->id;
		}

		public function getKey() {
			return $this->key;
		}

		public function getName() {
			return $this->name;
		}

		public function getDateStart() {
			return $this->dateStart;
		}

		public function getDateEnd() {
			return $this->dateEnd;
		}

		public function getDateDeadline() {
			return $this->dateDeadline;
		}

		public function getLocation() {
			return $this->location;
		}

		public function getDisciplines() {
			return $this->disciplines;
		}

		public function getLeagues() {
			return $this->leagues;
		}

		public function getWebsiteLink() {
			return $this->websiteLink;
		}

		public function isMultipleDays() {
			return ($this->dateStart !== $this->dateEnd);
		}

		public function hasWebsiteLink() {
			return ($this->websiteLink !== null);
		}
	}
?>