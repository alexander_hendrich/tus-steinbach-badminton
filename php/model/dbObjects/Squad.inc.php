<?php
	class Squad extends DBObject {
		### attributes
		private $id;
		private $key;
		private $name;
		private $report;
		private $alleturniereTournamentID;
		private $alleturniereTeamID;
		private $alleturnierePlayerID;

		private $malePlayers;
		private $femalePlayers;

		private $playerDB;


		### methodes

		public function __construct($playerDB) {
			$this->playerDB	= $playerDB;
		}

		public function loadDataFromSqlRow($rowData) {
			$this->id						= $rowData['id'];
			$this->key						= $rowData['key'];
			$this->name						= $rowData['name'];
			$this->report					= $rowData['report'];
			$this->alleturniereTournamentID	= $rowData['alleturniereTournamentID'];
			$this->alleturniereTeamID		= $rowData['alleturniereTeamID'];
			$this->alleturniereDrawID		= $rowData['alleturniereDrawID'];
		}

		public function getID() {
			return $this->id;
		}

		public function getKey() {
			return $this->key;
		}

		public function getName() {
			return $this->name;
		}

		public function getReport() {
			return $this->report;
		}

		public function getAlleturniereTournamentID() {
			return $this->alleturniereTournamentID;
		}

		public function getAlleturniereTeamID() {
			return $this->alleturniereTeamID;
		}

		public function getAlleturniereDrawID() {
			return $this->alleturniereDrawID;
		}

		public function getMalePlayers() {
			if($this->malePlayers === null) {
				$this->loadPlayers();
			}
			return $this->malePlayers;
		}

		public function getFemalePlayers() {
			if($this->femalePlayers === null) {
				$this->loadPlayers();
			}
			return $this->femalePlayers;
		}

		private function loadPlayers() {
			$this->malePlayers 		= $this->playerDB->getBySquadIDAndGender($this->id, 'male');
			$this->femalePlayers 	= $this->playerDB->getBySquadIDAndGender($this->id, 'female');
		}
	}
?>