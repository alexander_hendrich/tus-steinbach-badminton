<?php
	require_once('php/library/swiftMailer/swift_required.php');
			
	class MailHandler extends Swift_Mailer {
		### attributes
		
		### methodes
		
		public function __construct($transportAddress, $userName, $userPassword) {
			$swiftTransport = Swift_SmtpTransport::newInstance($transportAddress, 25);
			$swiftTransport->setUsername($userName);
			$swiftTransport->setPassword($userPassword);
			parent::__construct($swiftTransport);
		}
		
		public function getNewMailInstance() {
			return Swift_Message::newInstance();
		}
		
	}
?>