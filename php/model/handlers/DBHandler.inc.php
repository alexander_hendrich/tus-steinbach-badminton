<?php

	class DBHandler extends PDO {
		### attributes
		private $preparedStatements = array();
		
		### methodes
		
		public function __construct($host, $dbName, $userName, $userPasword) {
			//connect to db
			try {
				parent::__construct('mysql:host='.$host.';dbname='.$dbName.';charset=utf8', $userName, $userPasword);
				//set charset
				$this->query("SET NAMES 'utf8'");
				$this->query("SET CHARACTER SET 'utf8'");
			} catch(PDOException $e) {
				echo $e->getMessage();
				die();
			}
		}
		
		public function getPreparedStatement($queryString) {
			$queryHash = sha1($queryString);
		
			if($this->isCacheHit($queryHash)) {
				//get preparedStatement from cache
				$stmt = $this->getFromCache($queryHash);
			} else {
				//prepare statement
				$stmt = $this->prepare($queryString);
				$this->cache($queryHash, $stmt);
			}
			
			return $stmt;
		}
		
		private function isCacheHit($queryHash) {
			$isCacheHit = array_key_exists($queryHash, $this->preparedStatements);
			return $isCacheHit;
		}
		
		private function cache($queryHash, $stmt) {
			$this->preparedStatements[$queryHash] = $stmt;
		}
		
		private function getFromCache($queryHash) {
			return $this->preparedStatements[$queryHash];
		}
		
	}
?>