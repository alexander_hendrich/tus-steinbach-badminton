<?php
	class FeedbackHandler {
		### attributes
		private $feedback = array();
		
		### methodes
		
		public function newError($topic, $location, $key, $message) {
			$this->newfeedback('error', $topic, $location, $key, $message);
		}
		
		public function newConfirmation($topic, $location, $key, $message) {
			$this->newfeedback('confirmation', $topic, $location, $key, $message);
		}
		
		private function newfeedback($type, $topic, $location, $key, $message) {
			$this->feedback[$type][$topic][$location][$key] = $message;
		}
		
		public function isError($topic=null, $location=null, $key=null) {
			return $this->isfeedback('error', $topic, $location, $key);
		}
		
		public function isConfirmation($topic=null, $location=null, $key=null) {
			return $this->isfeedback('confirmation', $topic, $location, $key);
		}
		
		public function isfeedback($type=null, $topic=null, $location=null, $key=null) {
			$isfeedback = !empty($this->feedback);
			
			//type
			if($isfeedback && $type !== null) {
				$isfeedback = array_key_exists($type, $this->feedback);
				
				//topic
				if($isfeedback && $topic !== null) {
					$isfeedback = array_key_exists($topic, $this->feedback[$type]);
					
					//location
					if($isfeedback && $location !== null) {
						$isfeedback = array_key_exists($location, $this->feedback[$type][$topic]);
						
						//key
						if($isfeedback && $key !== null) {
							$isfeedback = array_key_exists($key, $this->feedback[$type][$topic][$location]) && $this->feedback[$type][$topic][$location][$key];
						}
					}
				}
			}
			
			return $isfeedback;
		}
		
		public function getfeedbackMessages($type, $topic, $location) {
			return $this->feedback[$type][$topic][$location];
		}
	}
?>