<?php
	class NotificationHandler {
		### attributes
		private $mailHandler;
		private $receiver;
		private $sender;



		### methodes

		public function __construct($mailHandler, $receiverEmail, $senderEmail) {
			$this->mailHandler	= $mailHandler;
			$this->receiver		= array($receiverEmail);
			$this->sender		= array($senderEmail);
		}

		public function notifyNewComment($comment) {
			//prepare message

			$authorName			= $comment->getAuthorName();
			$authorEMail		= $comment->getAuthorEMail();
			$text				= $comment->getText();
			$newsEntryHeadline	= $comment->getNewsEntry()->getHeadline();

			$subject	= 'Neuer Kommentar!';
			$message	= 	'Soeben hat "'.$authorName.'" ('.$authorEMail.') ein neuen Kommentar zum News-Beitrag "'.$newsEntryHeadline.'" verfasst.'.MAIL_EOL
							.MAIL_EOL.MAIL_DIVIDER.MAIL_EOL.MAIL_EOL
							.$text.MAIL_EOL
							.MAIL_EOL.MAIL_DIVIDER.MAIL_EOL.MAIL_EOL
							.'Dies ist eine automatisch generierte E-Mail.';

			//prepare mail
			$mail = $this->mailHandler->getNewMailInstance();
			$mail->setSubject($subject);
			$mail->setFrom($this->sender);
			$mail->setTo($this->receiver);
			$mail->setReplyTo($authorEMail);
			$mail->setBody($message);

			//send mail
			$this->mailHandler->send($mail);
		}

		public function notifyNewEntry($entry) {
			//prepare message
			$discipline = $entry->getDiscipline();

			$email			= $entry->getEMail();
			$group			= $discipline->getGroup();
			$type			= $discipline->getType();
			$disciplineName	= 'Gruppe '.$group.' - '.$type;
			$playerName		= $entry->getPlayer1Name();
			$playerSquad	= $entry->getPlayer1Squad();
			if($discipline->isDouble()) {
				$playerName		.= ' + '.$entry->getPlayer2Name();
				$playerSquad	.= ' + '.$entry->getPlayer2Squad();
			}

			$subject	= 'Neue Anmeldung!';
			$message	= 	'Soeben hat '.$email.' ein neuen Anmeldung für das Altkönigturnier abgeschickt.'.MAIL_EOL
							.MAIL_EOL.MAIL_DIVIDER.MAIL_EOL.MAIL_EOL
							.'Disziplin:	'.$disciplineName.MAIL_EOL
							.'Name:		'.$playerName.MAIL_EOL
							.'Verein:		'.$playerSquad.MAIL_EOL
							.MAIL_EOL.MAIL_DIVIDER.MAIL_EOL.MAIL_EOL
							.'Dies ist eine automatisch generierte E-Mail.';

			//send mail -> daniel
			$mail = $this->mailHandler->getNewMailInstance();
			$mail->setSubject($subject);
			$mail->setBody($message);
			$mail->setTo(array('altkoenigturnier@tus-steinbach-badminton.de' => 'Daniel Scheffler'));
			$mail->setFrom($email);
			$this->mailHandler->send($mail);

			//send mail -> alex
			$mail = $this->mailHandler->getNewMailInstance();
			$mail->setSubject($subject);
			$mail->setBody($message);
			$mail->setTo($this->receiver);
			$mail->setFrom($this->sender);
			$mail->setReplyTo($email);
			$this->mailHandler->send($mail);
		}

		public function notifyEntrySetStateAccepted($entry) {
			//prepare message
			$discipline = $entry->getDiscipline();

			$group			= $discipline->getGroup();
			$type			= $discipline->getType();
			$disciplineName	= 'Gruppe '.$group.' - '.$type;
			$email 			= $entry->getEMail();
			$playerName		= $entry->getPlayer1Name();
			$playerSquad	= $entry->getPlayer1Squad();
			if($discipline->isDouble()) {
				$playerName		.= ' + '.$entry->getPlayer2Name();
				$playerSquad	.= ' + '.$entry->getPlayer2Squad();
			}

			$subject	= 'Anmeldung akzeptiert!';
			$message	= 	'Die Turnierleitung hat soeben folgende Meldung akzeptiert:'.MAIL_EOL
							.MAIL_EOL.MAIL_DIVIDER.MAIL_EOL.MAIL_EOL
							.'Disziplin:	'.$disciplineName.MAIL_EOL
							.'Name:		'.$playerName.MAIL_EOL
							.'Verein:		'.$playerSquad.MAIL_EOL
							.MAIL_EOL.MAIL_DIVIDER.MAIL_EOL.MAIL_EOL
							.'Meldeliste:	http://tus-steinbach-badminton.de/altkoenigturnier/meldeliste/'.MAIL_EOL
							.MAIL_EOL.MAIL_DIVIDER.MAIL_EOL.MAIL_EOL
							.'Dies ist eine automatisch generierte E-Mail.';

			//send mail -> alex
			$mail = $this->mailHandler->getNewMailInstance();
			$mail->setSubject($subject);
			$mail->setBody($message);
			$mail->setTo($this->receiver);
			$mail->setFrom($this->sender);
			$this->mailHandler->send($mail);

			//send mail -> entryCreator
			$mail = $this->mailHandler->getNewMailInstance();
			$mail->setSubject($subject);
			$mail->setBody($message);
			$mail->setTo(array($email));
			$mail->setFrom($this->sender);
			$mail->setReplyTo('altkoenigturnier@tus-steinbach-badminton.de');
			$this->mailHandler->send($mail);
		}

		public function notifyEntrySetStateWaiting($entry) {
			//prepare message
			$discipline = $entry->getDiscipline();

			$group			= $discipline->getGroup();
			$type			= $discipline->getType();
			$disciplineName	= 'Gruppe '.$group.' - '.$type;
			$email 			= $entry->getEMail();
			$playerName		= $entry->getPlayer1Name();
			$playerSquad	= $entry->getPlayer1Squad();
			if($discipline->isDouble()) {
				$playerName		.= ' + '.$entry->getPlayer2Name();
				$playerSquad	.= ' + '.$entry->getPlayer2Squad();
			}

			$subject	= 'Anmeldung -> Warteliste';
			$message	= 	'Die Turnierleitung hat soeben folgende Meldung auf die Warteliste gesetzt:'.MAIL_EOL
							.MAIL_EOL.MAIL_DIVIDER.MAIL_EOL.MAIL_EOL
							.'Disziplin:	'.$disciplineName.MAIL_EOL
							.'Name:		'.$playerName.MAIL_EOL
							.'Verein:		'.$playerSquad.MAIL_EOL
							.MAIL_EOL.MAIL_DIVIDER.MAIL_EOL.MAIL_EOL
							.'Meldeliste:	http://tus-steinbach-badminton.de/altkoenigturnier/meldeliste/'.MAIL_EOL
							.MAIL_EOL.MAIL_DIVIDER.MAIL_EOL.MAIL_EOL
							.'Dies ist eine automatisch generierte E-Mail.';

			//send mail -> alex
			$mail = $this->mailHandler->getNewMailInstance();
			$mail->setSubject($subject);
			$mail->setBody($message);
			$mail->setTo($this->receiver);
			$mail->setFrom($this->sender);
			$this->mailHandler->send($mail);

			//send mail -> entryCreator
			$mail = $this->mailHandler->getNewMailInstance();
			$mail->setSubject($subject);
			$mail->setBody($message);
			$mail->setTo(array($email));
			$mail->setFrom($this->sender);
			$mail->setReplyTo('altkoenigturnier@tus-steinbach-badminton.de');
			$this->mailHandler->send($mail);
		}
	}
?>