<?php
	class ServiceFactory {
		### attributes
		private $dbHandler;

		private $mailHandler;
		private $notificationHandler;

		private $feedbackHandler;
		private $feedbackPresenter;

		private $commentDB;
		private $disciplineDB;
		private $entryDB;
		private $galleryDB;
		private $galleryImageDB;
		private $newsEntryDB;
		private $playerDB;
		private $squadDB;
		private $tournamentDB;
		private $userDB;


		### methodes

		public function getDBHandler() {
			if($this->dbHandler === null) {
				$host 			= DB_HANDLER_LOGIN_HOST;
				$dbName			= DB_HANDLER_LOGIN_DB_NAME;
				$userName		= DB_HANDLER_LOGIN_USER_NAME;
				$userPassword	= DB_HANDLER_LOGIN_USER_PASSWORD;
				$this->dbHandler = new DBHandler($host, $dbName, $userName, $userPassword);
			}
			return $this->dbHandler;
		}


		public function getMailHandler() {
			if($this->mailHandler == null) {
				$transportAddress 	= MAIL_HANDLER_TRANSPORT_ADDRESS;
				$userName			= MAIL_HANDLER_LOGIN_USER_NAME;
				$userPassword		= MAIL_HANDLER_LOGIN_USER_PASSWORD;
				$this->mailHandler = new MailHandler($transportAddress, $userName, $userPassword);
			}
			return $this->mailHandler;
		}

		public function getNotificationHandler() {
			if($this->notificationHandler === null) {
				$mailHandler 	= $this->getMailHandler();
				$receiverEMail 	= NOTIFICATION_HANDLER_RECEIVER_EMAIL;
				$senderEMail	= NOTIFICATION_HANDLER_SENDER_EMAIL;
				$this->notificationHandler = new NotificationHandler($mailHandler, $receiverEMail, $senderEMail);
			}
			return $this->notificationHandler;
		}

		public function getContactFormMessage() {
			$mailHandler = $this->getMailHandler();
			return new ContactFormMessage($mailHandler);
		}


		public function getFeedbackHandler() {
			if($this->feedbackHandler === null) {
				$this->feedbackHandler = new FeedbackHandler();
			}
			return $this->feedbackHandler;
		}

		public function getFeedbackPresenter() {
			if($this->feedbackPresenter === null) {
				$feedbackHandler = $this->getFeedbackHandler();
				$this->feedbackPresenter = new FeedbackPresenter($feedbackHandler);
			}
			return $this->feedbackPresenter;
		}


		public function getCommentDB() {
			if($this->commentDB === null) {
				$dbHandler 		= $this->getDBHandler();
				$tableName 		= TABLE_NAME_COMMENTS;
				$this->commentDB = new CommentDB($dbHandler, $tableName);

				$newsEntryDB = $this->getNewsEntryDB();
				$this->commentDB->setNewsEntryDB($newsEntryDB);
			}
			return $this->commentDB;
		}


		public function getDisciplineDB() {
			if($this->disciplineDB === null) {
				$dbHandler 		= $this->getDBHandler();
				$tableName 		= TABLE_NAME_DISCIPLINES;
				$this->disciplineDB = new DisciplineDB($dbHandler, $tableName);

				$entryDB = $this->getEntryDB();
				$this->disciplineDB->setEntryDB($entryDB);
			}
			return $this->disciplineDB;
		}

		public function getEntryDB() {
			if($this->entryDB === null) {
				$dbHandler 		= $this->getDBHandler();
				$tableName 		= TABLE_NAME_ENTRIES;
				$this->entryDB = new EntryDB($dbHandler, $tableName);

				$disciplineDB = $this->getDisciplineDB();
				$this->entryDB->setDisciplineDB($disciplineDB);
			}
			return $this->entryDB;
		}

		public function getGalleryDB() {
			if($this->galleryDB === null) {
				$dbHandler 		= $this->getDBHandler();
				$tableName 		= TABLE_NAME_GALLERIES;
				$this->galleryDB = new GalleryDB($dbHandler, $tableName);

				$galleryImageDB = $this->getGalleryImageDB();
				$this->galleryDB->setGalleryImageDB($galleryImageDB);
			}
			return $this->galleryDB;
		}

		public function getGalleryImageDB() {
			if($this->galleryImageDB === null) {
				$dbHandler 		= $this->getDBHandler();
				$tableName 		= TABLE_NAME_GALLERY_IMAGES;
				$this->galleryImageDB = new GalleryImageDB($dbHandler, $tableName);

				$galleryDB = $this->getGalleryDB();
				$this->galleryImageDB->setGalleryDB($galleryDB);
			}
			return $this->galleryImageDB;
		}

		public function getNewsEntryDB() {
			if($this->newsEntryDB === null) {
				$dbHandler	= $this->getDBHandler();
				$tableName	= TABLE_NAME_NEWS_ENTRIES;
				$this->newsEntryDB = new NewsEntryDB($dbHandler, $tableName);

				$commentDB = $this->getCommentDB();
				$galleryDB = $this->getGalleryDB();
				$this->newsEntryDB->setCommentDB($commentDB);
				$this->newsEntryDB->setGalleryDB($galleryDB);
			}
			return $this->newsEntryDB;
		}

		public function getPlayerDB() {
			if($this->playerDB === null) {
				$dbHandler	= $this->getDBHandler();
				$tableName	= TABLE_NAME_PLAYERS;
				$this->playerDB = new PlayerDB($dbHandler, $tableName);

				$squadDB = $this->getSquadDB();
				$this->playerDB->setSquadDB($squadDB);
			}
			return $this->playerDB;
		}

		public function getSquadDB() {
			if($this->squadDB === null) {
				$dbHandler	= $this->getDBHandler();
				$tableName	= TABLE_NAME_SQUADS;
				$this->squadDB = new SquadDB($dbHandler, $tableName);

				$playerDB = $this->getPlayerDB();
				$this->squadDB->setPlayerDB($playerDB);
			}
			return $this->squadDB;
		}

		public function getTournamentDB() {
			if($this->tournamentDB === null) {
				$dbHandler	= $this->getDBHandler();
				$tableName	= TABLE_NAME_TOURNAMENTS;
				$this->tournamentDB = new TournamentDB($dbHandler, $tableName);
			}
			return $this->tournamentDB;
		}

		public function getUserDB() {
			if($this->userDB === null) {
				$dbHandler	= $this->getDBHandler();
				$tableName	= TABLE_NAME_USERS;
				$this->userDB = new UserDB($dbHandler, $tableName);
			}
			return $this->userDB;
		}
	}
?>