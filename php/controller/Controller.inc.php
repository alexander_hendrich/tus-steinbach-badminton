<?php
	class Controller {
		### attributes
		protected $view;

		### methodes

		public function __construct($view) {
			$this->view = $view;
		}

		public function display() {
			echo $this->view->getHtml();
		}

	}
?>