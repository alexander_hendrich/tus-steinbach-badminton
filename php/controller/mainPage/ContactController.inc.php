<?php
	class ContactController extends MainPageController {
		### attributes
		private $contactFormMessage;
		private $feedbackHandler;
		private $pageData;

		### methodes

		public function __construct($view, $contactFormMessage, $feedbackHandler) {
			parent::__construct($view);
			$this->contactFormMessage	= $contactFormMessage;
			$this->feedbackHandler 		= $feedbackHandler;

			//handle Requests
			$this->handleRequests();

			//set pageData
			$this->setPageData();
		}

		private function setPageData() {
			//sendContactFormDefaultValues
			if(isset($_POST['sendContactForm']) && $this->feedbackHandler->isError('sendContactForm')) {
				$sendContactFormDefaultValues = array(
					'receiver'	=> $_POST['sendContactFormReceiver'],
					'name'		=> $_POST['sendContactFormName'],
					'email'		=> $_POST['sendContactFormEMail'],
					'subject'	=> $_POST['sendContactFormSubject'],
					'message'	=> $_POST['sendContactFormMessage']
				);
			} else {
				$sendContactFormDefaultValues = array(
					'receiver'	=> '',
					'name'		=> '',
					'email'		=> '',
					'subject'	=> '',
					'message'	=> ''
				);
			}

			$this->view->setPageData($sendContactFormDefaultValues);
		}

		private function handleRequests() {
			if(isset($_POST['sendContactForm'])) {
				$this->handleSendContactForm();
			}
		}

		private function handleSendContactForm() {

			//check spam
			try {
				$this->spamCheck();
			} catch(SpamException $e) {
				$this->feedbackHandler->newError('sendContactForm', 'general', 'spam', 'Spam erkannt! Bitte warten Sie ein paar Sekunden und versuchen Sie es dann erneut.');
			}

			//receiver
			try {
				$this->contactFormMessage->setReceiver($_POST['sendContactFormReceiver']);
			} catch(InvalidContactFormReceiverException $e) {
				$this->feedbackHandler->newError('sendContactForm', 'receiver', 'invalid', 'Ungültiger Empfänger. Bitte wählen Sie einen Empfänger aus der Liste aus.');
			}

			//name
			try {
				$this->contactFormMessage->setName($_POST['sendContactFormName']);
			} catch(EmptyException $e) {
				$this->feedbackHandler->newError('sendContactForm', 'name', 'empty', 'Bitte geben Sie Ihren Namen ein.');
			} catch(InvalidCharactersException $e) {
				$this->feedbackHandler->newError('sendContactForm', 'name', 'invalidCharacters', 'Ungültige Zeichen.');
			}

			//email
			try {
				$this->contactFormMessage->setEMail($_POST['sendContactFormEMail']);
			} catch(EmptyException $e) {
				$this->feedbackHandler->newError('sendContactForm', 'email', 'empty', 'Bitte geben Sie Ihre E-Mail-Adresse ein.');
			} catch(InvalidEMailException $e) {
				$this->feedbackHandler->newError('sendContactForm', 'email', 'invalid', 'Bitte geben Sie eine gültige E-Mail-Adresse ein.');
			}

			//subject
			try {
				$this->contactFormMessage->setSubject($_POST['sendContactFormSubject']);
			} catch(EmptyException $e) {
				$this->feedbackHandler->newError('sendContactForm', 'subject', 'empty', 'Bitte geben Sie einen Betreff ein.');
			}

			//message
			try {
				$this->contactFormMessage->setMessage($_POST['sendContactFormMessage']);
			} catch(EmptyException $e) {
				$this->feedbackHandler->newError('sendContactForm', 'message', 'empty', 'Bitte geben Sie Ihre Nachricht ein.');
			}


			if($this->feedbackHandler->isError('sendContactForm') === false) {
				//send contactForm
				$this->contactFormMessage->send();

				//give confirmation
				$this->feedbackHandler->newConfirmation('sendContactForm', 'general', 'success', 'Ihre Nachricht wurde erfolgreich verschickt.');
			} else {
				$this->feedbackHandler->newError('sendContactForm', 'general', 'failure', 'Es ist ein Fehler aufgetreten. Bitte überprüfen Sie Ihre Eingabe.');
			}
		}
	}
?>