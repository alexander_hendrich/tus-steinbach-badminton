<?php
	class TournamentsController extends MainPageController {
		### attributes
		private $tournamentDB;

		### methodes

		public function __construct($view, $tournamentDB) {
			parent::__construct($view);
			$this->tournamentDB = $tournamentDB;

			//set pageData
			$this->setPageData();
		}

		private function setPageData() {
			$tournaments = $this->tournamentDB->getFutureTournaments();

			$this->view->setPageData($tournaments);
		}
	}
?>