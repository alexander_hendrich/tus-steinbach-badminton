<?php
	class MainPageController extends Controller {
		### attributes
		protected $view;

		### methodes

		protected function spamCheck() {
			//check time
			$start		= $_POST[SPAM_CHECK_TIME_NAME];
			$end		= time();
			$timeNeeded	= $end - $start;

			if($timeNeeded > SPAM_CHECK_MAX_TIME || $timeNeeded < SPAM_CHECK_MIN_TIME) {
				throw new SpamException();
			}

			//check honeypot
			if(empty($_POST[SPAM_CHECK_HONEYPOT_NAME]) === false) {
				throw new SpamException();
			}
		}
	}
?>