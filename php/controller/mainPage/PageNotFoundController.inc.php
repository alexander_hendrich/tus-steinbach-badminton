<?php
	class PageNotFoundController extends MainPageController {
		### attributes

		### methodes

		public function __construct($view) {
			parent::__construct($view);

			//send 404-header
			header("HTTP/1.0 404 Not Found");
		}
	}
?>