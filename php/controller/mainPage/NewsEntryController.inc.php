<?php
	class NewsEntryController extends MainPageController {
		### attributes
		private $newsEntryDB;
		private $notificationHandler;
		private $feedbackHandler;

		### methodes

		public function __construct($view, $newsEntryDB, $commentDB, $notificationHandler, $feedbackHandler) {
			parent::__construct($view);
			$this->newsEntryDB 			= $newsEntryDB;
			$this->commentDB			= $commentDB;
			$this->notificationHandler	= $notificationHandler;
			$this->feedbackHandler 		= $feedbackHandler;

			//handle requests
			$this->handleRequests();

			//set pageData
			$this->setpageData();
		}

		private function setPageData() {
			//check if valid newsEntry
			if(isset($_GET['newsEntryKey']) && $this->newsEntryDB->isValidKey($_GET['newsEntryKey'])) {
				//newsEntry
				$newsEntry = $this->newsEntryDB->getByKey($_GET['newsEntryKey']);

				//addCommentDefaultValues
				if(isset($_POST['addComment']) && $this->feedbackHandler->isError('addComment')) {
					$addCommentDefaultValues = array(
						'newsEntryID'	=> $newsEntry->getID(),
						'authorName'	=> $_POST['addCommentAuthorName'],
						'authorEMail'	=> $_POST['addCommentAuthorEMail'],
						'text'			=> $_POST['addCommentText']
					);
				} else {
					$addCommentDefaultValues = array(
						'newsEntryID'	=> $newsEntry->getID(),
						'authorName'	=> '',
						'authorEMail'	=> '',
						'text'			=> ''
					);
				}

				$this->view->setPageData($newsEntry, $addCommentDefaultValues);
			} else {
				throw new NewsEntryNotFoundException();
			}
		}

		private function handleRequests() {
			if(isset($_POST['addComment'])) {
				$this->handleAddCommentRequest();
			}
		}

		private function handleAddCommentRequest() {
			$newComment = $this->commentDB->createEmptyObject();

			//check spam
			try {
				$this->spamCheck();
			} catch(SpamException $e) {
				$this->feedbackHandler->newError('addComment', 'general', 'spam', 'Spam erkannt! Bitte warten Sie ein paar Sekunden und versuchen Sie es dann erneut.');
			}

			//set newsEntryID
			try {
				$newComment->setNewsEntryID($_POST['addCommentNewsEntryID']);
			} catch(InvalidIDException $e) {
				$this->feedbackHandler->newError('addComment', 'general', 'invalidNewsEntryID', 'Ungültige NewsEntryID.');
			}

			//set dateTime
			$newComment->setDateTime(time());

			//set authorName
			try {
				$newComment->setAuthorName($_POST['addCommentAuthorName']);
			} catch(EmptyException $e) {
				$this->feedbackHandler->newError('addComment', 'authorName', 'empty', 'Bitte geben Sie Ihren Namen ein.');
			} catch(InvalidCharactersException $e) {
				$this->feedbackHandler->newError('addComment', 'authorName', 'invalidCharacters', 'Ungültige Zeichen.');
			}

			//set authorEMail
			try {
				$newComment->setAuthorEMail($_POST['addCommentAuthorEMail']);
			} catch(EmptyException $e) {
				$this->feedbackHandler->newError('addComment', 'authorEMail', 'empty', 'Bitte geben Sie Ihre E-Mail-Adresse ein.');
			} catch(InvalidEMailException $e) {
				$this->feedbackHandler->newError('addComment', 'authorEMail', 'invalid', 'Bitte geben Sie eine gültige E-Mail-Adresse ein.');
			}

			//set text
			try {
				$newComment->setText($_POST['addCommentText']);
			} catch(EmptyException $e) {
				$this->feedbackHandler->newError('addComment', 'text', 'empty', 'Bitte geben Sie einen Kommentar.');
			}

			if($this->feedbackHandler->isError('addComment') === false) {
				//add to db
				$this->commentDB->add($newComment);

				//notify admin
				$this->notificationHandler->notifyNewComment($newComment);

				//give confirmation
				$this->feedbackHandler->newConfirmation('addComment', 'general', 'success', 'Ihr Kommentar wurde erfolgreich hinzugefügt.');
			} else {
				$this->feedbackHandler->newError('addComment', 'general', 'failure', 'Es ist ein Fehler aufgetreten. Bitte überprüfen Sie Ihre Eingabe.');
			}
		}
	}
?>