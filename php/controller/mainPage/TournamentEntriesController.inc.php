<?php
	class TournamentEntriesController extends MainPageController {
		### attributes
		private $disciplineDB;
		private $entryDB;
		private $notificationHandler;
		private $feedbackHandler;

		### methodes

		public function __construct($view, $disciplineDB, $entryDB, $notificationHandler, $feedbackHandler) {
			parent::__construct($view);
			$this->disciplineDB			= $disciplineDB;
			$this->entryDB				= $entryDB;
			$this->notificationHandler	= $notificationHandler;
			$this->feedbackHandler		= $feedbackHandler;

			//handle requests
			$this->handleRequests();

			//set pageData
			$this->setPageData();
		}

		protected function setPageData() {
			//disciplines
			$disciplines = $this->disciplineDB->getAll();

			//addEntryDefaultValues
			if(isset($_POST['addEntry']) && $this->feedbackHandler->isError('addEntry')) {
				$addEntryDefaultValues = array(
					'disciplineID'	=> $_POST['addEntryDisciplineID'],
					'email'			=> $_POST['addEntryEMail'],
					'player1Name'	=> $_POST['addEntryPlayer1Name'],
					'player2Name'	=> $_POST['addEntryPlayer2Name'],
					'player1Squad'	=> $_POST['addEntryPlayer1Squad'],
					'player2Squad'	=> $_POST['addEntryPlayer2Squad']
				);
			} else {
				$addEntryDefaultValues = array(
					'disciplineID'	=> $disciplines[0]->getID(),
					'email'			=> '',
					'player1Name'	=> '',
					'player2Name'	=> '',
					'player1Squad'	=> '',
					'player2Squad'	=> ''
				);
			}

			$this->view->setPageData($disciplines, $addEntryDefaultValues);
		}

		protected function handleRequests() {
			if(isset($_POST['addEntry'])) {
				$this->handleAddEntryRequest();
			}
		}

		private function handleAddEntryRequest() {
			$newEntry = $this->entryDB->createEmptyObject();
			$isDouble = false;

			//check spam
			try {
				$this->spamCheck();
			} catch(SpamException $e) {
				$this->feedbackHandler->newError('addEntry', 'general', 'spam', 'Spam erkannt! Bitte warten Sie ein paar Sekunden und versuchen Sie es dann erneut.');
			}

			//set disciplineID
			try {
				$newEntry->setDisciplineID($_POST['addEntryDisciplineID']);
				$isDouble = $newEntry->getDiscipline()->isDouble();
			} catch(InvalidIDException $e) {
				$this->feedbackHandler->newError('addEntry', 'disciplineID', 'invalidDisciplineID', 'Bitte wählen Sie eine der Disziplinen aus der Liste aus.');
			}

			//set state
			$newEntry->setState('pending');

			//set email
			try {
				$newEntry->setEMail($_POST['addEntryEMail']);
			} catch(EmptyException $e) {
				$this->feedbackHandler->newError('addEntry', 'email', 'empty', 'Bitte geben Sie Ihre E-Mail-Adresse ein!');
			} catch(InvalidEMailException $e) {
				$this->feedbackHandler->newError('addEntry', 'authorEMail', 'invalid', 'Bitte geben Sie eine gültige E-Mail-Adresse ein.');
			}

			//set player1Name
			try {
				$newEntry->setPlayer1Name($_POST['addEntryPlayer1Name']);
			} catch(EmptyException $e) {
				$this->feedbackHandler->newError('addEntry', 'player1Name', 'empty', 'Bitte geben Sie einen Namen ein!');
			}

			//set player1Squad
			try {
				$newEntry->setPlayer1Squad($_POST['addEntryPlayer1Squad']);
			} catch(EmptyException $e) {
				$this->feedbackHandler->newError('addEntry', 'player1Squad', 'empty', 'Bitte geben Sie einen Verein ein!');
			}

			if($isDouble) {
				//set player2Name
				try {
					$newEntry->setPlayer2Name($_POST['addEntryPlayer2Name']);
				} catch(EmptyException $e) {
					$this->feedbackHandler->newError('addEntry', 'player2Name', 'empty', 'Bitte geben Sie einen Namen ein!');
				}

				//set player2Squad
				try {
					$newEntry->setPlayer2Squad($_POST['addEntryPlayer2Squad']);
				} catch(EmptyException $e) {
					$this->feedbackHandler->newError('addEntry', 'player2Squad', 'empty', 'Bitte geben Sie einen Verein ein!');
				}
			}

			if($this->feedbackHandler->isError('addEntry') === false) {
				//add to db
				$this->entryDB->add($newEntry);

				//notify admin
				$this->notificationHandler->notifyNewEntry($newEntry);

				//give confirmation
				$this->feedbackHandler->newConfirmation('addEntry', 'general', 'success', 'Ihre Anmeldung war erfolgreich. Sie erhalten eine Bestätigung per E-Mail sobald die Turnierleitung Ihre Anmeldung überpüft hat.');
			} else {
				$this->feedbackHandler->newError('addEntry', 'general', 'failure', 'Es ist ein Fehler aufgetreten. Bitte überprüfen Sie Ihre Eingabe.');
			}
		}
	}
?>