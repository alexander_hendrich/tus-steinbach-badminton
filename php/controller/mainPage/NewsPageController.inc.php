<?php
	class NewsPageController extends MainPageController {
		### attributes
		private $newsEntryDB;

		### methodes

		public function __construct($view, $newsEntryDB) {
			parent::__construct($view);
			$this->newsEntryDB = $newsEntryDB;

			//set pageData
			$this->setPageData();
		}

		protected function setPageData() {
			$maxPageNum = ceil($this->newsEntryDB->getCountAll() / NEWS_PAGE_NUM_NEWS_ENTRIES_PER_PAGE);

			//check if valid pageNum
			if(isset($_GET['pageNum']) && is_numeric($_GET['pageNum']) && $_GET['pageNum'] >= 1 && $_GET['pageNum'] <= $maxPageNum) {
				$pageNum		= $_GET['pageNum'];
				$newsEntries	= $this->newsEntryDB->getByPageNum($_GET['pageNum'], NEWS_PAGE_NUM_NEWS_ENTRIES_PER_PAGE);

				$this->view->setPageData($newsEntries, $pageNum, $maxPageNum);
			} else {
				throw new InvalidNewsPageNumberException();
			}
		}
	}
?>