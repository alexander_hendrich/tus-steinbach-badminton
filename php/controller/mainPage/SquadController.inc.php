<?php
	class SquadController extends MainPageController {
		### attributes
		private $squadDB;

		### methodes

		public function __construct($view, $squadDB) {
			parent::__construct($view);
			$this->squadDB = $squadDB;

			//set pageData
			$this->setPageData();
		}

		protected function setPageData() {
			//check if valid squad
			if(isset($_GET['squadKey']) && $this->squadDB->isValidKey($_GET['squadKey'])) {
				//squad
				$squad = $this->squadDB->getByKey($_GET['squadKey']);

				$this->view->setPageData($squad);
			} else {
				throw new SquadNotFoundException();
			}
		}
	}
?>