<?php
	class AdminPageController extends Controller {
		### attributes
		protected $user;

		protected $feedbackHandler;
		protected $notificationHandler;

		### methodes

		public function __construct($view, $feedbackHandler, $notificationHandler, $user) {
			parent::__construct($view);
			$this->feedbackHandler		= $feedbackHandler;
			$this->notificationHandler	= $notificationHandler;
			$this->user					= $user;

		}
	}
?>