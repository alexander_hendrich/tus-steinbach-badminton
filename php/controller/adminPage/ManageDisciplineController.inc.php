<?php
	class ManageDisciplineController extends AdminPageController {
		### attributes
		private $disciplineDB;
		private $entryDB;

		### methodes

		public function __construct($view, $feedbackHandler, $notificationHandler, $disciplineDB, $entryDB, $user) {
			parent::__construct($view, $feedbackHandler, $notificationHandler, $user);
			$this->disciplineDB	= $disciplineDB;
			$this->entryDB		= $entryDB;

			//handle requests
			$this->handleRequests();

			//set pageData
			$this->setPageData();
		}

		private function setPageData() {
			if($this->disciplineDB->isValidID($_GET['disciplineID'])) {
				//discipline
				$pageData['discipline'] = $this->disciplineDB->getByID($_GET['disciplineID']);

				$this->view->setPageData($pageData);
			} else {
				throw new DisciplineNotFoundException();
			}
		}

		private function handleRequests() {
			if(isset($_POST['entrySetStateAccepted'])) {
				$this->handleEntrySetStateAcceptedRequest();
			}
			if(isset($_POST['entrySetStateWaiting'])) {
				$this->handleEntrySetStateWaitingRequest();
			}
		}

		private function handleEntrySetStateAcceptedRequest() {
			if($this->entryDB->isValidID($_POST['entryID'])) {
				$entry = $this->entryDB->getByID($_POST['entryID']);

				//set state
				try {
					$entry->setState('accepted');
				} catch(DisciplineFullException $e) {
					$this->feedbackHandler->newError('entrySetStateAccepted', 'general', 'disciplineFull', 'Die ausgewählte Disziplin ist bereits voll. Es können keine weiteren Meldungen mehr angenommen werden.');
				}

			} else {
				$this->feedbackHandler->newError('entrySetStateAccepted', 'general', 'invalidID', 'Ungültige ID!');
			}

			if($this->feedbackHandler->isError('entrySetStateAccepted') === false) {
				//update db
				$entry->notifyObservers();

				//notify admin
				$this->notificationHandler->notifyEntrySetStateAccepted($entry);

				//give confirmation
				$this->feedbackHandler->newConfirmation('entrySetStateAccepted', 'general', 'success', 'Die Meldung wurde erfolgreich angenommen.');
			} else {
				$this->feedbackHandler->newError('entrySetStateAccepted', 'general', 'failure', 'Es ist ein Fehler aufgetreten!');
			}
		}

		private function handleEntrySetStateWaitingRequest() {
			if($this->entryDB->isValidID($_POST['entryID'])) {
				$entry = $this->entryDB->getByID($_POST['entryID']);

				//set state
				$entry->setState('waiting');

			} else {
				$this->feedbackHandler->newError('entrySetStateWaiting', 'general', 'invalidID', 'Ungültige ID!');
			}

			if($this->feedbackHandler->isError('entrySetStateWaiting') === false) {
				//update db
				$entry->notifyObservers();

				//notify admin
				$this->notificationHandler->notifyEntrySetStateWaiting($entry);

				//give confirmation
				$this->feedbackHandler->newConfirmation('entrySetStateWaiting', 'general', 'success', 'Die Meldung wurde erfolgreich auf die Warteliste gesetzt.');
			} else {
				$this->feedbackHandler->newError('entrySetStateWaiting', 'general', 'failure', 'Es ist ein Fehler aufgetreten!');
			}
		}
	}
?>