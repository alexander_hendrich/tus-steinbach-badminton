<?php
	function classLoader($className) {
		switch($className) {

			// php/controller/
			case 'Controller':
			case 'ServiceFactory':
				require_once('php/controller/'.$className.'.inc.php');
				break;

			// php/controller/adminPage/
			case 'AdminPageController':
			case 'ManageDisciplineController':
				require_once('php/controller/adminPage/'.$className.'.inc.php');
				break;

			// php/controller/mainPage/
			case 'ContactController':
			case 'MainPageController':
			case 'NewsEntryController':
			case 'NewsPageController':
			case 'PageNotFoundController':
			case 'SquadController':
			case 'TournamentEntriesController':
			case 'TournamentResultsController':
			case 'TournamentsController':
				require_once('php/controller/mainPage/'.$className.'.inc.php');
				break;

			// php/exceptions
			case 'DisciplineFullException':
			case 'DisciplineNotFoundException':
			case 'EmptyException':
			case 'IllegalIDOverrideException':
			case 'InvalidCharactersException':
			case 'InvalidContactFormReceiverException':
			case 'InvalidDateTimeException':
			case 'InvalidEMailException':
			case 'InvalidEntryStateException':
			case 'InvalidIDException':
			case 'InvalidNewsPageNumberException':
			case 'NewsEntryNotFoundException':
			case 'NotImplementedException':
			case 'NotLoggedInException':
			case 'PageNotFoundException':
			case 'SpamException':
			case 'SquadNotFoundException':
				require_once('php/exceptions/'.$className.'.inc.php');
				break;

			// php/library/observerPattern/
			case 'Observable':
			case 'IObserver':
				require_once('php/library/observerPattern/'.$className.'.inc.php');
				break;

			// php/model/
			case 'ContactFormMessage':
				require_once('php/model/'.$className.'.inc.php');
				break;

			// php/model/dbObjects/
			case 'Comment':
			case 'DBObject':
			case 'Discipline':
			case 'Entry':
			case 'Gallery':
			case 'GalleryImage':
			case 'NewsEntry':
			case 'NewsPage':
			case 'Player':
			case 'Squad':
			case 'Tournament':
			case 'User':
				require_once('php/model/dbObjects/'.$className.'.inc.php');
				break;


			// php/model/handlers/
			case 'DBHandler':
			case 'FeedbackHandler':
			case 'MailHandler':
			case 'NotificationHandler':
				require_once('php/model/handlers/'.$className.'.inc.php');
				break;

			// php/model/objectDBs/
			case 'CommentDB':
			case 'DisciplineDB':
			case 'EntryDB':
			case 'GalleryDB':
			case 'GalleryImageDB':
			case 'KeyObjectDB':
			case 'NewsEntryDB':
			case 'ObjectDB':
			case 'PlayerDB':
			case 'SquadDB':
			case 'TournamentDB':
			case 'UserDB':
				require_once('php/model/objectDBs/'.$className.'.inc.php');
				break;

			// php/model/objectDBs/sqlQueries/
			case 'DeleteQuery':
			case 'InsertQuery':
			case 'SelectQuery':
			case 'SqlQuery':
			case 'UpdateQuery':
				require_once('php/model/objectDBs/sqlQueries/'.$className.'.inc.php');
				break;

			// php/view/htmlViews/
			case 'HtmlView':
				require_once('php/view/htmlViews/'.$className.'.inc.php');
				break;

			// php/view/htmlViews/adminPage/
			case 'AdminPageHtmlView':
			case 'ManageDisciplineHtmlView':
			case 'LoginHtmlView':
				require_once('php/view/htmlViews/adminPage/'.$className.'.inc.php');
				break;

			// php/view/htmlViews/mainPage/
			case 'ContactHtmlView':
			case 'ImpressumHtmlView':
			case 'MainPageHtmlView':
			case 'NewsEntryHtmlView':
			case 'NewsPageHtmlView':
			case 'PageNotFoundHtmlView':
			case 'SquadHtmlView':
			case 'TournamentAnnouncementHtmlView':
			case 'TournamentEntriesHtmlView':
			case 'TournamentsHtmlView':
			case 'TournamentResultsHtmlView':
			case 'TrainingHtmlView':
				require_once('php/view/htmlViews/mainPage/'.$className.'.inc.php');
				break;

			// php/view/objectUIs/
			case 'CommentUI':
			case 'DisciplineUI':
			case 'EntryUI':
			case 'GalleryImageUI':
			case 'GalleryUI':
			case 'NewsEntryUI':
			case 'PlayerUI':
			case 'SquadUI':
			case 'TournamentUI':
				require_once('php/view/objectUIs/'.$className.'.inc.php');
				break;

			// php/view/presenters/
			case 'FeedbackPresenter':
				require_once('php/view/presenters/'.$className.'.inc.php');
				break;
		}
	}
?>