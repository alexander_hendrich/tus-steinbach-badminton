<?php
	class Observable {
		### attributes
		private $hasChanged = false;
		private $observers = array();

		### methodes

		public function notifyObservers($arg=null) {
			if($this->hasChanged) {
				foreach($this->observers as $observer) {
					$observer->update($this);
				}
				$this->hasChanged = false;
			}
		}

		public function addObserver($observer) {
			$this->observers[] = $observer;
		}

		public function removeObserver($observer) {
			//find index
			$removeIndex = null;
			foreach($this->observers as $index=>$observerAtIndex) {
				if($observerAtIndex = $observer) {
					$removeIndex = $index;
				}
			}

			//remove observer
			unset($this->observers[$removeIndex]);
		}

		protected function setChanged() {
			$this->hasChanged = true;
		}
	}
?>