<?php
	Interface IObserver {
		public function update($object);
	}
?>