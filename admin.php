<?php
	//start session
	session_start();

	//init classLoader
	require_once('php/classLoader.inc.php');
	spl_autoload_register('classLoader');

	//load config
	require_once('php/config.inc.php');

	$serviceFactory = new ServiceFactory();

	$notificationHandler	= $serviceFactory->getNotificationHandler();
	$feedbackHandler		= $serviceFactory->getFeedbackHandler();
	$feedbackPresenter		= $serviceFactory->getFeedbackPresenter();

	//check login
	$loggedIn	= false;
	$userDB		= $serviceFactory->getUserDB();
	if(isset($_SESSION['userID']) && $userDB->isValidID($_SESSION['userID'])) {
		//already logged in
		$loggedIn	= true;
		$user		= $userDB->getByID($_SESSION['userID']);
	} else if(isset($_POST['login'])) {
		//new login
		$user = $userDB->getByEMail($_POST['loginEMail']);
		if($user !== null && $user->checkPassword($_POST['loginPassword'])) {
			//successfull login
			$loggedIn			= true;
			$_SESSION['userID']	= $user->getID();
		} else {
			//login failed
			$user = null;
			$feedbackHandler->newError('login', 'general', 'invalidLogin', 'Login fehlgeschlagen!');
		}
	}

	if($loggedIn) {
		$disciplineDB	= $serviceFactory->getDisciplineDB();
		$disciplines	= $disciplineDB->getAll();

		try {
			switch($_GET['page']) {
				case 'manageDiscipline':
					$entryDB = $serviceFactory->getEntryDB();

					$view		= new ManageDisciplineHtmlView($feedbackPresenter, $disciplines);
					$controller	= new ManageDisciplineController($view, $feedbackHandler, $notificationHandler, $disciplineDB, $entryDB, $user);
					break;

				default:
					throw new PageNotFoundException();
					break;
			}

		} catch(PageNotFoundException $e) {
			$view		= new AdminPageHtmlView($feedbackPresenter, $disciplines);
			$controller	= new Controller($view);
		}

	} else {
		$view		= new LoginHtmlView($feedbackPresenter);
		$controller	= new Controller($view);
	}



	$controller->display();
/*
	$feedbackHandler	= $serviceFactory->getFeedbackHandler();
	$feedbackPresenter	= $serviceFactory->getFeedbackPresenter();
	$userDB				= $serviceFactory->getUserDB();
	$disciplineDB		= $serviceFactory->getDisciplineDB();

	try {
		switch($_GET['page']) {
			default:
				$view = new AdminPageHtmlView($feedbackPresenter);
				$controller = new AdminPageController($view, $feedbackHandler, $userDB, $disciplineDB);
				break;
		}
	} catch(NotLoggedInException $e) {
		$view = new LoginHtmlView();
		$controller = new LoginController();
	}
*/

?>