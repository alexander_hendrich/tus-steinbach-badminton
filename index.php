<?php
	//init classLoader
	require_once('php/classLoader.inc.php');
	spl_autoload_register('classLoader');

	//load config
	require_once('php/config.inc.php');

	$serviceFactory = new ServiceFactory();

	try {
		switch($_GET['page']) {
			//contact
			case 'contact':
				$contactFormMessage	= $serviceFactory->getContactFormMessage();
				$feedbackHandler	= $serviceFactory->getFeedbackHandler();

				$feedbackPresenter	= $serviceFactory->getFeedbackPresenter();
				$view				= new ContactHtmlView($feedbackPresenter);

				$controller = new ContactController($view, $contactFormMessage, $feedbackHandler);
				break;

			//impressum
			case 'impressum':
				$impressumHtmlView = new ImpressumHtmlView();

				$controller = new MainPageController($impressumHtmlView);
				break;

			//newsEntry
			case 'newsEntry':
				$newsEntryDB			= $serviceFactory->getNewsEntryDB();
				$commentDB				= $serviceFactory->getCommentDB();
				$notificationHandler	= $serviceFactory->getNotificationHandler();
				$feedbackHandler		= $serviceFactory->getFeedbackHandler();

				$feedbackPresenter	= $serviceFactory->getFeedbackPresenter();
				$view				= new NewsEntryHtmlView($feedbackPresenter);

				$controller = new NewsEntryController($view, $newsEntryDB, $commentDB, $notificationHandler, $feedbackHandler);
				break;

			//newsPage
			case 'newsPage':
				$newsEntryDB = $serviceFactory->getNewsEntryDB();

				$view	= new NewsPageHtmlView();

				$controller	= new NewsPageController($view, $newsEntryDB);
				break;

			//squad
			case 'squad':
				$squadDB = $serviceFactory->getSquadDB();

				$view = new SquadHtmlView();

				$controller = new SquadController($view, $squadDB);
				break;

			//tournmanetAnnouncement
			case 'tournamentAnnouncement':
				$view = new TournamentAnnouncementHtmlView();

				$controller = new MainPageController($view);
				break;

			case 'tournamentEntries':
				$disciplineDB			= $serviceFactory->getDisciplineDB();
				$entryDB				= $serviceFactory->getEntryDB();
				$notificationHandler	= $serviceFactory->getNotificationHandler();
				$feedbackHandler		= $serviceFactory->getFeedbackHandler();

				$feedbackPresenter	= $serviceFactory->getFeedbackPresenter();
				$view				= new TournamentEntriesHtmlView($feedbackPresenter);

				$controller = new TournamentEntriesController($view, $disciplineDB, $entryDB, $notificationHandler, $feedbackHandler);
				break;

			case 'tournamentResults':
				$view = new TournamentResultsHtmlView();

				$controller = new MainPageController($view);
				break;

			//tournaments
			case 'tournaments':
				$tournamentDB = $serviceFactory->getTournamentDB();

				$view = new TournamentsHtmlView();

				$controller = new TournamentsController($view, $tournamentDB);
				break;

			//training
			case 'training':
				$view = new TrainingHtmlView();

				$controller = new MainPageController($view);
				break;

			//pageNotFound
			default:
				throw new PageNotFoundException();
				break;
		}
	}
	catch(PageNotFoundException $e) {
		$view = new PageNotFoundHtmlView();

		$controller = new PageNotFoundController($view);
	}

	$controller->display();

?>